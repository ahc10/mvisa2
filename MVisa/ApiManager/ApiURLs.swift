//
//  Constants.swift
//  Scarlet
//
//  Created by Ali Merhie on 4/30/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation

//MARK: URL here:
//let HOST_URL = "http://192.168.12.130:9090"
let HOST_URL = "https://virtualcard.montymobile.com"

let API_URL = HOST_URL + "/api/v1/"

let SignUp_API = API_URL + "user/signUp"
let SignIn_API = API_URL + "user/login"
let Logout_API = API_URL + "user/logout"
let DeviceInfo_API = API_URL + "userDevice"
let ForgetPass_API = API_URL + "user/forgetPassword"
let VerifyForgetPass_API = API_URL + "user/verifyForget"
let ResetPass_API = API_URL + "user/resetPassword"
let ForgetPassTimeRemaining_API = API_URL + "user/remainingForgetTime/"
let ResendForgetPassOtp_API = API_URL + "user/resendForgetCode/"
let RefreshToken_API = API_URL + "user/refreshToken"
let Verify_API = API_URL + "user/verify"
let ResendOtp_API = API_URL + "user/resendOTP/"
let TimeRemaining_API = API_URL + "user/remainingTime/"
let Claim_API = API_URL + "cards/claim"
let Cards_API = API_URL + "cards"
let CardInfo_API = API_URL + "cards/"
let DeactivateCard_API = API_URL + "cards/deactivate"
let FreezeState_API = API_URL + "cards/cardState/"
let ClaimState_API = API_URL + "cards/cardInfo"
let Trasnfers_API = API_URL + "transfers"
let Payments_API = API_URL + "cards/getPayments"
let Balance_API = API_URL + "balance"
let AirTime_API = API_URL + "topup/airTime"
let ChangePass_API = API_URL + "changePassword"
let FAQ_API = API_URL + "faq"
let ContactUs_API = API_URL + "help"
let Terms_API = API_URL + "termsAndCondition"
let ScratchCard_API = API_URL + "cards/scratchCard"
let Pay_API = API_URL + "cards/payments/qr"
