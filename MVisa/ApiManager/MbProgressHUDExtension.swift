//
//  MbProgressHUDExtension.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/3/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//


import Foundation
import MBProgressHUD
import Toast_Swift

extension UIViewController {
    func showHud(_ message: String = "Loading") {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        //  hud.setDefaultMaskType(.clear)
        hud.label.text = message
        hud.isUserInteractionEnabled = true
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
