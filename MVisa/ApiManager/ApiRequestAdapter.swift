//
//  ApiRequestResponseAdapter.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/3/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Alamofire
import MBProgressHUD
import SkeletonView

class ApiRequestAdapter: RequestAdapter {
    private let accessToken: String
    private let prefix: String
    
    
    public init(accessToken: String, prefix: String) {
        self.accessToken = accessToken
        self.prefix = prefix
        
    }
    
    public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        let reachable = NetworkReachabilityManager()?.isReachable ?? false
        if !reachable {
            Shared.hideSkeletonAnimation()
            UIApplication.getTopViewController()?.hideHUD()
            Shared.displayAlert(title: "noConnection".localized(), message: "noConnectionDesc".localized(), cancelText: "done".localized())
            let sessionManager = Alamofire.SessionManager.default
            sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                dataTasks.forEach { $0.cancel() }
                uploadTasks.forEach { $0.cancel() }
                downloadTasks.forEach { $0.cancel() }
            }


        }
        
        var urlRequest = urlRequest

        
        //for upload dont add any header and dont show loader
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(prefix) {
            //loading is stopped in the responder (ApiResponderAdapter)
            
//            let loaderType: String = urlRequest.value(forHTTPHeaderField: "loader") ?? LoadingType.Loader.rawValue
//            if Thread.isMainThread{
//                if loaderType == LoadingType.Loader.rawValue {
//                    UIApplication.getTopViewController()?.showHud("loading")
//                    //WindowHelper.shared.window.rootViewController
//                }
//                else if loaderType == LoadingType.Skeleton.rawValue {
//                    Shared.startSkeletonAnimation()
//                }
//            }
            urlRequest.setValue( "ios:\(DeviceHelper.appVersion)", forHTTPHeaderField: "app-version")
            urlRequest.setValue( SelectedLanguageDefualt.id, forHTTPHeaderField: "Accept-Language")


            if !urlString.hasPrefix(SignUp_API) {
                urlRequest.setValue( "Bearer " + accessToken, forHTTPHeaderField: "Authorization")
            }
//            if  urlString.hasPrefix(UploadImage_API)  {
//                return urlRequest
//            }
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
        }
        //check if token is refreshing then stop the request and add to cache
        //   if ApiResponseAdapter.isRefreshing {
        //            Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
        //                sessionDataTask.forEach { $0.suspend() }
        //                uploadData.forEach { $0.suspend() }
        //                downloadData.forEach { $0.suspend() }
        //            }
        //  ApiResponseAdapter.cachedTasks.append(urlRequest)
        // }
        return urlRequest
    }
}

//extension SessionManager  {
//
//override open func request(
//    _ url: URLConvertible,
//    method: HTTPMethod = .get,
//    parameters: Parameters? = nil,
//    encoding: ParameterEncoding = URLEncoding.default,
//    headers: HTTPHeaders? = nil)
//    -> DataRequest
//{
//    var originalRequest: URLRequest?
//    
//    do {
//        originalRequest = try URLRequest(url: url, method: method, headers: headers)
//        let encodedURLRequest = try encoding.encode(originalRequest!, with: parameters)
//        return request(encodedURLRequest)
//    } catch {
//        return request(originalRequest, failedWith: error)
//    }
//}
//
//}
