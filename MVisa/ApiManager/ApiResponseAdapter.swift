//
//  ApiResponseAdapter.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/3/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftKeychainWrapper


class ApiResponseAdapter: RequestRetrier {
    
    //    public typealias NetworkSuccessHandler = (AnyObject?) -> Void
    //    public typealias NetworkFailureHandler = (HTTPURLResponse?, AnyObject?, NSError) -> Void
    //
    //    private typealias CachedTask = (HTTPURLResponse?, AnyObject?, NSError?) -> Void
    //
    //    public static var cachedTasks = Array<URLRequest>()
    public static var isRefreshing = false
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        print("REtrierrrr")
        
        if let response = request.task?.response as? HTTPURLResponse{
            
            if response.statusCode == 401 {
                print("caseRetrier 401 \((response.url?.absoluteString) ?? "")")

                if !isLoggedInDefualts {
                    completion(false, 0.0)
                    return
                }
                
                let urlString = (response.url?.absoluteString) ?? ""
                if !urlString.hasPrefix(RefreshToken_API) && !ApiResponseAdapter.isRefreshing  {
                    refreshToken()
                }
                print("retry count: \(request.retryCount)")
                if request.retryCount == 5 {
                    if urlString.hasPrefix(RefreshToken_API){
                        DispatchQueue.main.async {
                            Shared.logOut()
                            Shared.showToast(text: "Session Expired!")
                        }
                    }
                    completion(false, 0.0)
                    return
                }
                completion(true, 1.0)
                return
            }else if response.statusCode == 403{
                print("caseRetrier 403  \((response.url?.absoluteString) ?? "")")
                DispatchQueue.main.async {
                    let sessionManager = Alamofire.SessionManager.default
                    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                        dataTasks.forEach { $0.cancel() }
                        uploadTasks.forEach { $0.cancel() }
                        downloadTasks.forEach { $0.cancel() }
                        
                    }
                    Shared.expireSession()
                            }
                completion(false, 0.0)// don't retry
                return
            }else {
                completion(false, 0.0)// don't retry
                return
            }
        }
    }
    private func refreshToken(){
        print("Refresh Token")
        
        ApiResponseAdapter.isRefreshing = true
        ApiServices.shared.refreshToken { (result, statusCode) in
            ApiResponseAdapter.isRefreshing = false
            if statusCode == 200 {
                Shared.updateTokens(token: result.token ?? "", refreshToken: result.refreshToken ?? "")
            }
        }
    }
}

extension DataRequest {
    
    @discardableResult
    public func responseObject<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: DataRequest.ObjectMapperSerializer(keyPath, mapToObject: object, context: context), completionHandler: completionHandler)
    }
    
    @discardableResult
    public func responseObjectCustom<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        
        // let vc = AppDelegate.shared.window?.rootViewController
        
        responseJSON { response in
            
            self.handelStatusResponse(statusCode: response.response?.statusCode ?? 1,  request: response.request!)
        }
        let responseCustom = responseObject{ (response: DataResponse<T>) in
            completionHandler(response)
        }
        return responseCustom
    }
    
    @discardableResult
    public func responseArrayCustom<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        
        responseJSON { response in
            self.handelStatusResponse(statusCode: response.response?.statusCode ?? 1, request: response.request!)
            
        }
        let responseCustom = responseArray{ (response: DataResponse<[T]>) in
            completionHandler(response)
        }
        return responseCustom
    }
    
    private func handelStatusResponse(statusCode: Int,  request: URLRequest){
        self.responseJSON { (responseJson) in
            print("--------------------------------API REQUEST----------------------------------------")
            print("STATUS: \(statusCode)")
            print("REQUEST:\(request)")
            
            print("RESPONSE: \(responseJson)")
            print("---------------------------------------------------------------------")
        }
        let urlString = (request.url?.absoluteString) ?? ""
        
        switch statusCode {
        case 200:
            print("case 200")
            //case 301:
        //Shared.forceUpgrade()
        case 400:
            print("case 400")
            self.responseObject{ (response: DataResponse<ResponseModel<ErrorModel>>) in
                let error = response.value?.data
                Shared.displayAlert(title: error?.error ?? "InApp error: ApiResponderAdapter", message: error?.error_description ?? "Unknown error", cancelText: "Dismiss")
            }
        case 401:
            print("case 401")
            //Shared.displayAlert(title: "sessionExpired".localized(), message: "willLogOut".localized(), cancelText: "dismiss".localized())
            //Shared.logOut()
            
        case 500:
            print("Server Error - 500")
            //if !urlString.hasPrefix(UploadImage_API){
            Shared.displayAlert(title: "serverError".localized(), message: "serverErrorDesc".localized(), cancelText: "Dismiss")
        //}
        case 1:
            print("case Unknown - No response")
            //if !urlString.hasPrefix(UploadImage_API){
            let  title = "Error"
            // Shared.displayAlert(title: title, message: "serverErrorDesc".localized(), cancelText: "dismiss".localized())
            // }
            
        default:
            print("case default: \( statusCode)")
        }
        //remove hud after request is done
        if Thread.isMainThread{
            // LoaderHelper.hideCustomLoader()
            //remove hud after request is done
            //UIApplication.getTopViewController()?.hideHUD()
            //UIApplication.getTopViewController()?.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.5))
        }
    }
    
    
}
