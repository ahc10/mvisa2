//
//  ApiServices.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/3/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//


import Foundation
import AlamofireObjectMapper
import Alamofire
import SwiftKeychainWrapper
import ObjectMapper
import XMLMapper
import TIFeedParser

final class ApiServices {
    var token: String
    static var shared = ApiServices()
    static let reachabilityManager = NetworkReachabilityManager()
    let statusCodeRange = 200...400
    
    private init() {
        token = TokenKeyChain
        SessionManager.default.adapter = ApiRequestAdapter(accessToken: token, prefix: API_URL)
        SessionManager.default.retrier = ApiResponseAdapter()
        
        //handle internet connection / in ApiRequestAdapter if no internet connection the rwquest will be suspended once the connection is back will resume the request here
        ApiServices.reachabilityManager?.listener = { [weak self] status in
            var isReachable = false
            switch status {
            case .notReachable, .unknown:
                isReachable = false
                print("disconnected")
                break
            case .reachable:
                isReachable = true
                print("connected")
                NotificationCenter.default.post(name: .internetConnectionBack, object: nil)
                
                break
            }
            // Notify the rest of the app (perhaps use a Notification)
        }
        ApiServices.reachabilityManager?.startListening()
        
    }
    public static func reInitialize(){
        shared = ApiServices()
    }
    public static func getDeviceInfoHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = HTTPHeaders()
        headers["fcmToken"] = FcmTokenDefualts
        headers["deviceId"] = DeviceHelper.deviceUniqueId
        headers["osVersion"] = DeviceHelper.systemVersion
        headers["phoneName"] = DeviceHelper.name
        headers["os"] = DeviceHelper.systemName
        headers["model"] = DeviceHelper.modelString
        headers["brand"] = "Apple"
        headers["mnc"] = DeviceHelper.mobileNetworkCode
        headers["mcc"] = DeviceHelper.mobileCountryCode
        headers["appVersion"] = DeviceHelper.appVersion
        headers["systemInfo"] = DeviceHelper.systemInfo
        headers["operatorName"] = DeviceHelper.CarrierName

        return headers
    }
    func signUp(firstName: String, lastName: String, phoneCode: String, phoneNumber: String, password: String, completion: @escaping (UserModel, Int) -> Void) {
        let url = URL(string: SignUp_API)!
        
        let headers = ApiServices.getDeviceInfoHeader()
        
        var params = [String : Any]()
        params["firstName"] = firstName
        params["lastName"] = lastName
        params["phoneCode"] = phoneCode
        params["phoneNumber"] = phoneNumber
        params["password"] = password
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.value?.data ?? UserModel(), response.response?.statusCode ?? 1)
        }
    }
    func updateDeviceInfo(completion: @escaping (Int) -> Void) {
        let url = URL(string: DeviceInfo_API)!
        
        let headers = ApiServices.getDeviceInfoHeader()
        
        let req = request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func verify(userId: Int, otpCode: String, completion: @escaping (UserModel, Int) -> Void) {
        let url = URL(string: Verify_API)!
        
        
        var params = [String : Any]()
        params["userId"] = userId
        params["otpCode"] = otpCode
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.value?.data ?? UserModel(), response.response?.statusCode ?? 1)
        }
    }
    func login(phoneNumber: String, password: String, completion: @escaping (UserModel, Int) -> Void) {
        let url = URL(string: SignIn_API)!
        
        let headers = ApiServices.getDeviceInfoHeader()
        
        var params = [String : Any]()
        params["phoneNumber"] = phoneNumber
        params["password"] = password
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.value?.data ?? UserModel(), response.response?.statusCode ?? 1)
        }
    }
    func logout(completion: @escaping (Int) -> Void) {
        let url = URL(string: Logout_API)!

        let req = request(url, method: .post, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func refreshToken(completion: @escaping (UserModel, Int) -> Void) {
        let url = URL(string: RefreshToken_API)!
        
        var headers: HTTPHeaders = HTTPHeaders()
        headers["refresh-token"] = RefreshTokenKeyChain
        
        
        let req = request(url, method: .post, encoding: JSONEncoding.default, headers: headers )
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.value?.data ?? UserModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func forgetPaasword(phoneNumber: String, completion: @escaping (Int) -> Void) {
        let url = URL(string: ForgetPass_API)!
        
        var params = [String : Any]()
        params["phoneNumber"] = phoneNumber
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func verifyForgetPaasword(phoneNumber: String, code: String, completion: @escaping (Int) -> Void) {
        let url = URL(string: VerifyForgetPass_API)!
        
        
        var params = [String : Any]()
        params["phoneNumber"] = phoneNumber
        params["otpCode"] = code
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func resetPassword(phoneNumber: String, code: String, newPass: String, confirmPass: String, completion: @escaping (Int) -> Void) {
        let url = URL(string: ResetPass_API)!
        
        
        var params = [String : Any]()
        params["phoneNumber"] = phoneNumber
        params["resetToken"] = code
        params["newPassword"] = newPass
        params["newConfirmedPassword"] = confirmPass
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func getForgetPassResendTimeRemaining(phoneNumber: String, completion: @escaping (TimeModel, Int) -> Void) {
        let urlwithPercentEscapes = "\(ForgetPassTimeRemaining_API)\(phoneNumber)".addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? ""
        let url = URL(string: urlwithPercentEscapes)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TimeModel>>) in
            
            completion(response.value?.data ?? TimeModel(), response.response?.statusCode ?? 1)
        }
    }
    func resendForgetPassOtp(phoneNumber: String, completion: @escaping (Int) -> Void) {
        let url = URL(string: "\(ResendForgetPassOtp_API)\(phoneNumber)".addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? "")!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TimeModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func resendOtp(completion: @escaping (Int) -> Void) {
        let url = URL(string: "\(ResendOtp_API)\(UserInfoDefualts.userId ?? 0)")!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<UserModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }
    }
    func getResendTimeRemaining(completion: @escaping (TimeModel, Int) -> Void) {
        let url = URL(string: "\(TimeRemaining_API)\(UserInfoDefualts.userId ?? 0)")!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TimeModel>>) in
            
            completion(response.value?.data ?? TimeModel(), response.response?.statusCode ?? 1)
        }
    }
    func changePassword(oldPass: String ,newPass: String, completion: @escaping (Int) -> Void) {
        let url = URL(string: ChangePass_API)!
        
        var params = [String : Any]()
        params["oldPassword"] = oldPass
        params["newPassword"] = newPass
        
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TransfersModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func claimCard(completion: @escaping (TimeModel, Int) -> Void) {
        let url = URL(string: Claim_API)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TimeModel>>) in
            
            completion(response.value?.data ?? TimeModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getCards(completion: @escaping (CardsModel, Int) -> Void) {
        let url = URL(string: Cards_API)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<CardsModel>>) in
            
            completion(response.value?.data ?? CardsModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getCardInfo(cardId: Int, completion: @escaping (CardInfoMainModel, Int) -> Void) {
        let url = URL(string: "\(CardInfo_API)\(cardId ?? 0)")!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<CardInfoMainModel>>) in
            
            completion(response.value?.data ?? CardInfoMainModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func DeactivateCard(cardId: Int, completion: @escaping (Int) -> Void) {
        let url = URL(string: DeactivateCard_API)!
        
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<CardInfoMainModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func ChangeFreezeState(cardId: Int, freeze: Bool, completion: @escaping (Int) -> Void) {
        let url = URL(string: "\(FreezeState_API)\(cardId ?? 0)/\(freeze ? 0 : 1)")!
        
        let req = request(url, method: .put, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<CardInfoMainModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getClaimState(completion: @escaping (ClaimStateModel, Int) -> Void) {
        let url = URL(string: ClaimState_API)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<ClaimStateModel>>) in
            
            completion(response.value?.data ?? ClaimStateModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getTrasnfers(from: String, to: String, completion: @escaping (TransfersModel, Int) -> Void) {
        let url = URL(string: Trasnfers_API)!
        
        var params = [String : Any]()
        params["fromDate"] = from
        params["toDate"] = to
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TransfersModel>>) in
            
            completion(response.value?.data ?? TransfersModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getPaymentts(from: String, to: String, completion: @escaping (PaymentsModel, Int) -> Void) {
        let url = URL(string: Payments_API)!
        
        var params = [String : Any]()
        params["fromDate"] = from
        params["toDate"] = to
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<PaymentsModel>>) in
            
            completion(response.value?.data ?? PaymentsModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getPhoneBalance(completion: @escaping (BalanceModel, Int) -> Void) {
        let url = URL(string: Balance_API)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<BalanceModel>>) in
            
            completion(response.value?.data ?? BalanceModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func topUpAirtime(amount: Double, completion: @escaping (BalanceModel, Int) -> Void) {
        let url = URL(string: AirTime_API)!
        
        var params = [String : Any]()
        params["amount"] = amount
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<BalanceModel>>) in
            
            completion(response.value?.data ?? BalanceModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func topUpScratchCard(scratchCard: String, completion: @escaping (BalanceModel, Int) -> Void) {
        let url = URL(string: ScratchCard_API)!
        
        var params = [String : Any]()
        params["scratchCard"] = scratchCard
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<BalanceModel>>) in
            
            completion(response.value?.data ?? BalanceModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getFAQs(completion: @escaping (FaqsModel, Int) -> Void) {
        let url = URL(string: FAQ_API)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<FaqsModel>>) in
            
            completion(response.value?.data ?? FaqsModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func contactUs(title: String, message: String, completion: @escaping (Int) -> Void) {
        let url = URL(string: ContactUs_API)!
        
        var params = [String : Any]()
        params["title"] = title
        params["message"] = message
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TransfersModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func getTerms(completion: @escaping (TermsModel, Int) -> Void) {
        let url = URL(string: Terms_API)!
        
        let req = request(url, method: .get, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TermsModel>>) in
            
            completion(response.value?.data ?? TermsModel(), response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }
    func payAmount(data: QRData, completion: @escaping (Int) -> Void) {
        let url = URL(string: Pay_API)!
        
        var params = data.dictionary
        
        let req = request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
        
        req.responseObjectCustom { (response: DataResponse<ResponseModel<TransfersModel>>) in
            
            completion(response.response?.statusCode ?? 1)
        }.validate(statusCode: statusCodeRange)
    }

}
