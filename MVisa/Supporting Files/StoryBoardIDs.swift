//
//  StoryBoardIDs.swift
//  PalmReading
//
//  Created by Ali Merhie on 8/5/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation

let Luanch_VC = ViewControllerModel(viewControllerIdentifier: "LuanchViewController", storyBoardIdentifier: "Luanch")
let Main_VC = ViewControllerModel(viewControllerIdentifier: "MainViewController", storyBoardIdentifier: "Main")
let SignIn_VC = ViewControllerModel(viewControllerIdentifier: "SignInViewController", storyBoardIdentifier: "SignIn")
let Register_VC = ViewControllerModel(viewControllerIdentifier: "RegisterViewController", storyBoardIdentifier: "Register")
let Verify_VC = ViewControllerModel(viewControllerIdentifier: "VerifyViewController", storyBoardIdentifier: "Verify")
let ChooseDate_VC = ViewControllerModel(viewControllerIdentifier: "ChooseDateViewController", storyBoardIdentifier: "ChooseDate")
let PaymentTransfer_VC = ViewControllerModel(viewControllerIdentifier: "PaymentTransferViewController", storyBoardIdentifier: "PaymentTransfer")
let CardInfo_VC = ViewControllerModel(viewControllerIdentifier: "CardInfoViewController", storyBoardIdentifier: "CardInfo")
let CardExplain_VC = ViewControllerModel(viewControllerIdentifier: "CardExplainViewController", storyBoardIdentifier: "CardExplain")
let ClaimCard_VC = ViewControllerModel(viewControllerIdentifier: "ClaimCardViewController", storyBoardIdentifier: "ClaimCard")
let TopupOptions_VC = ViewControllerModel(viewControllerIdentifier: "TopUpOptionsViewController", storyBoardIdentifier: "TopUpOptions")
let ScratcCard_VC = ViewControllerModel(viewControllerIdentifier: "ScratcCardViewController", storyBoardIdentifier: "ScratcCard")
let TopUpSuccess_VC = ViewControllerModel(viewControllerIdentifier: "TopUpSuccessViewController", storyBoardIdentifier: "TopUpSuccess")
let AirtimeTransfer_VC = ViewControllerModel(viewControllerIdentifier: "AirtimeTransferViewController", storyBoardIdentifier: "AirtimeTransfer")
let ConfirmAirtimeTransfer_VC = ViewControllerModel(viewControllerIdentifier: "ConfirmAirTimeTransferViewController", storyBoardIdentifier: "AirtimeTransfer")
let PayOptions_VC = ViewControllerModel(viewControllerIdentifier: "PayOptionsViewController", storyBoardIdentifier: "PayOptions")
let QRScan_VC = ViewControllerModel(viewControllerIdentifier: "QRScanViewController", storyBoardIdentifier: "QRScan")
let More_VC = ViewControllerModel(viewControllerIdentifier: "MoreViewController", storyBoardIdentifier: "More")
let MyProfile_VC = ViewControllerModel(viewControllerIdentifier: "MyProfileViewController", storyBoardIdentifier: "MyProfile")
let ChangePassword_VC = ViewControllerModel(viewControllerIdentifier: "ChangePasswordViewController", storyBoardIdentifier: "ChangePassword")
let Help_VC = ViewControllerModel(viewControllerIdentifier: "HelpViewController", storyBoardIdentifier: "Help")
let FAQAnswer_VC = ViewControllerModel(viewControllerIdentifier: "FAQAnswerViewController", storyBoardIdentifier: "FAQ")
let FAQ_VC = ViewControllerModel(viewControllerIdentifier: "FAQViewController", storyBoardIdentifier: "FAQ")
let Pay_VC = ViewControllerModel(viewControllerIdentifier: "PayViewController", storyBoardIdentifier: "Pay")
let AlertConfirm_VC = ViewControllerModel(viewControllerIdentifier: "AlertViewController", storyBoardIdentifier: "Alert")
let Terms_VC = ViewControllerModel(viewControllerIdentifier: "TermsViewController", storyBoardIdentifier: "Terms")
let TermsConfirm_VC = ViewControllerModel(viewControllerIdentifier: "TermsConfirmViewController", storyBoardIdentifier: "Terms")
let ForgetPassword_VC = ViewControllerModel(viewControllerIdentifier: "ForgetPasswordViewController", storyBoardIdentifier: "ForgetPassword")





