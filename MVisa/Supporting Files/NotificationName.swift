//
//  NotificationName.swift
//  CoronaCare
//
//  Created by Ali Merhie on 4/14/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let internetConnectionBack = Notification.Name("internetConnectionBack")
    static let willEnterForeground = Notification.Name("willEnterForeground")
    static let didAnswerQuestions = Notification.Name("didAnswerQuestions")
    static let updateMainPageData = Notification.Name("updateMainPageData")

}
