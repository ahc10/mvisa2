//
//  Shared.swift
//  PalmReading
//
//  Created by Ali Merhie on 8/8/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit
import ActionSheetPicker_3_0
import  ObjectMapper
import UserNotifications
import MobileCoreServices
import SwiftEntryKit
import PopMenu
import Contacts
import FirebaseMessaging
import Firebase
import AVFoundation
import Photos
import SkeletonView
import CallKit
import SemiModalViewController
import Toast_Swift

class Shared {
    static var  notificationCenter = UNUserNotificationCenter.current()
    
    public static func logIn(userData: UserModel) {
        // UserInfoDefualts = userData
        isLoggedInDefualts = true
        updateTokens(token: UserInfoDefualts.token ?? "", refreshToken: UserInfoDefualts.refreshToken ?? "")
        ScreenLoader().setRootNavigationController(viewController: Main_VC)
        //        Shared.navigateWithNavigationBar(viewController: TabBar_VC)
        //        updateFcmToken()
    }
    public static func updateUserInfo(userData: UserModel) {
        UserInfoDefualts = userData
        updateTokens(token: UserInfoDefualts.token ?? "", refreshToken: UserInfoDefualts.refreshToken ?? "")
    }
    public static func updateTokens(token: String, refreshToken: String = RefreshTokenKeyChain){
        TokenKeyChain = token
        RefreshTokenKeyChain = refreshToken
        ApiServices.reInitialize()
        
    }
    public static func logOut() {
        UserInfoDefualts = UserModel()
        TokenKeyChain = ""
        RefreshTokenKeyChain = ""
        isLoggedInDefualts = false
        ScreenLoader().setRootNavigationController(viewController: SignIn_VC)
        ApiServices.shared.logout { (statusCode) in
            
        }
        
    }
    public static func expireSession() {
        Shared.logOut()
        Shared.showToast(text: "Session Expired!")
        
    }
    
    public static func vibrate(){
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    //setup navigation attributes
    public static func setupNavigationAttributes (vc: UIViewController ) -> UIViewController {
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.barTintColor = UIColor.white
        //let attributes = [NSAttributedString.Key.font: UIFont(name: "SFProText-Medium", size: 17)!]
        //UINavigationBar.appearance().titleTextAttributes = attributes
        return nav
    }
    public static func startSkeletonAnimation(view: UIView = UIApplication.getTopViewController()!.view){
        //let gradient = SkeletonGradient(baseColor: UIColor(named: "charcoalGrey")!)
        view.showAnimatedGradientSkeleton()
    }
    public static func hideSkeletonAnimation(view: UIView = UIApplication.getTopViewController()!.view){
        view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.5))
    }
    
    
    public static func scheduleLocalNotification(alert:String) {
        let content = UNMutableNotificationContent()
        let requestIdentifier = UUID.init().uuidString
        
        content.badge = 0
        content.title = "Location Update"
        content.body = alert
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            print("Notification Register Success")
        }
    }
    public static func scheduleLocalNotification(title: String, message: String, Date: DateComponents, identifier: String, repeats: Bool){
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                self.notificationCenter.requestAuthorization(options: options) {
                    (didAllow, error) in
                    if !didAllow {
                        print("User has declined notifications")
                    }
                }
            }
        }
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.sound = UNNotificationSound.default
        content.badge = 0
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: Date, repeats: repeats)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    
    @objc static func openSetting(sender: AnyObject) {
        let currentVc = WindowHelper.shared.window.rootViewController as! UINavigationController
        
        let menuViewController = PopMenuViewController(sourceView: sender, actions: [
            PopMenuDefaultAction(title: "Languages", didSelect: { action in
                currentVc.dismiss(animated: true, completion: nil)
                
            }),
            PopMenuDefaultAction(title: "Log Out", didSelect: { action in
                currentVc.dismiss(animated: true, completion: nil)
                // Shared.logOut()
                
            })
        ])
        
        currentVc.present(menuViewController, animated: true, completion: nil)
        
    }
    
    public static  var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00") as TimeZone?
        return formatter
    }()
    public static func customiseNavigation(view: UIViewController){
        
        view.navigationController?.navigationBar.shouldRemoveShadow(true)
        view.navigationController?.navigationBar.isTranslucent = false
        view.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let backImage = UIImage(named: "BackArrow")!.withRenderingMode(.alwaysTemplate) as UIImage?
        view.navigationController?.navigationBar.tintColor = UIColor.white
        view.navigationController?.navigationBar.backIndicatorImage = backImage
        view.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        view.navigationController?.navigationBar.backItem?.title = ""
    }
    public static func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    public static func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    public static func displayConfirmAlert(title: String, message: String, confirmText: String, cancelText: String, completion: @escaping (Bool) -> () ) {
        let vc = UIApplication.getTopViewController()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: cancelText, style: .destructive,  handler: { (action) in completion(false) })
        let confirm = UIAlertAction(title: confirmText, style: .default, handler: { (action) in completion(true) })
        
        alert.addAction(cancel)
        alert.addAction(confirm)
        
        vc?.present(alert, animated: true, completion: nil)
    }
    
    
    public static func displayAlert(title: String, message: String, cancelText: String) {
        let vc = UIApplication.getTopViewController()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: cancelText, style: .cancel, handler: nil)
        alert.addAction(cancel)
        vc?.present(alert, animated: true, completion: nil)
    }
    public static func accessCameraImages(source: ImageSource , completion: @escaping (Bool) -> Void) {
        switch source {
        case .camera:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    completion(true)
                    print("Permission granted, proceed")
                } else {
                    completion(false)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Can't access contact", message: "Please go to Settings -> MyApp to enable contact permission", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        WindowHelper.shared.window.rootViewController!.present(alert, animated: true, completion: nil)
                    }
                }
            }
        case .photoLibrary:
            PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                if status == .authorized{
                    completion(true)
                    
                }else{
                    completion(false)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Can't access contact", message: "Please go to Settings -> MyApp to enable contact permission", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        WindowHelper.shared.window.rootViewController!.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
            
        }
        
    }
    public static func accessContacts(completion: @escaping (Bool, CNContactStore) -> Void) {
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts, completionHandler: {
            granted, error in
            
            guard granted else {
                
                completion(false, CNContactStore())
                
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Can't access contact", message: "Please go to Settings -> Privacy -> Contacts -> MontyCatch to enable contact permission", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    WindowHelper.shared.window.rootViewController!.present(alert, animated: true, completion: nil)
                    
                }
                
                return
            }
            completion(true, store)
        })
        
    }
    public static func inviteToApp(number: String){
        let sms: String = "sms:\(number)&body=Download MontyCatch and check your callers.Also rate your freinds.\nhttps://montycatch.page.link/catch"
        let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)
        
    }
    public static func shareLink(link: String){
        if let name = URL(string: link), !name.absoluteString.isEmpty {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            UIApplication.getTopViewController()?.present(activityVC, animated: true, completion: nil)
        }else  {
            // show alert for not available
        }
    }
    public static func showToast(text: String){
        var style = ToastStyle()
        style.backgroundColor = UIColor(named: "toastColor")!
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
        
        ToastManager.shared.style = style
        WindowHelper.shared.window.makeToast(text)
        
    }
    
    public static func updateDeviceInfo(){
        //retreive the messaging token locally
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                FcmTokenDefualts = result.token
                if isLoggedInDefualts{
                    ApiServices.shared.updateDeviceInfo { (statusCode) in
                        
                    }
                }
                //self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
    }
}



