//
//  Constants.swift
//  PalmReading
//
//  Created by Ali Merhie on 8/14/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation

let GoogleApiKey = "AIzaSyA9ExJ_V0J1RfXlE9foKD-_O71uv36tP7E"
let kLogsFile = "LocationLogs"
let kLogsDirectory = "LocationData"
let regionRadius = 200.0
let distanceFilter = 50.0

enum ImageSource {
    case photoLibrary
    case camera
}

enum LoadingType: String {
    case None = "None"
    case Loader = "Loader"
    case Skeleton = "Skeleton"
}

enum SilentPushActionEnum: Int {
    case Logout = 0
    case RefreshMain = 1
}
