//
//  CardInfoModel.swift
//  MVisa
//
//  Created by Ali Merhie on 6/1/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class CardInfoModel: Mappable, Encodable {
    var card_number: String?
    var cvv: String?
    var expiry_date: String?
    var card_holder_name: String?
    var time_till_expiry: Int?
    var tokenization_id: String?

    init() {
        
    }
    
    required init?(map: Map) {
        card_number <- map["card_number"]
        cvv <- map["cvv"]
        expiry_date <- map["expiry_date"]
        card_holder_name <- map["card_holder_name"]
        time_till_expiry <- map["time_till_expiry"]
        tokenization_id <- map["tokenization_id"]
    }
    
    func mapping(map: Map) {
        
    }
}
class CardInfoMainModel: Mappable, Encodable {
    var card: CardInfoModel?


    init() {
        
    }
    
    required init?(map: Map) {
        card <- map["card"]

    }
    
    func mapping(map: Map) {
        
    }
}
