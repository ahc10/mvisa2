//
//  TransferModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/29/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class TransferModel: Mappable, Encodable {
    var id: String?
    var msisdn: String?
    var amount: Double?
    var currency: String?
    var createdDate: String?
    var isScratchCard: Bool?
    var scratchCard: String?

    init() {
        
    }
    
    required init?(map: Map) {
        id <- map["id"]
        msisdn <- map["msisdn"]
        amount <- map["amount"]
        currency <- map["currency"]
        createdDate <- map["createdDate"]
        isScratchCard <- map["isScratchCard"]
        scratchCard <- map["scratchCard"]

    }
    
    func mapping(map: Map) {
        
    }
    
}
class TransfersModel: Mappable, Encodable {
    var trasnfers: [TransferModel] = []
    
    init() {
        
    }
    
    required init?(map: Map) {
        trasnfers <- map["transfers"]
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
