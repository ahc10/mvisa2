//
//  CardModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/29/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class CardModel: Mappable, Encodable {
    var cardId: Int?
    var holderName: String?
    var expiryDate: String?
    var last4Digits: String?
    var cvv: String?
    var cardBalance: Double?
    var dueAmount: Double?
    var outstandingAmount: Double?
    var dueDate: String?
    var currency: String?
    var subscriptionDueAmount: Double?
    var subscriptionDueDate: String?
    var state: Int?
    var isFreezed: Bool = true
    var payments: [PaymentModel] = []
    var transfers: [TransferModel] = []

    init() {
        
    }
    
    required init?(map: Map) {
        cardId <- map["cardId"]
        holderName <- map["holderName"]
        expiryDate <- map["expiryDate"]
        last4Digits <- map["last4Digits"]
        cvv <- map["cvv"]
        cardBalance <- map["cardBalance"]
        dueAmount <- map["dueAmount"]
        outstandingAmount <- map["outstandingAmount"]
        dueDate <- map["dueDate"]
        currency <- map["currency"]
        subscriptionDueAmount <- map["subscriptionDueAmount"]
        subscriptionDueDate <- map["subscriptionDueDate"]
        state <- map["state"]
        isFreezed <- map["isFreezed"]
        payments <- map["payments"]
        transfers <- map["transfers"]

        
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
class CardsModel: Mappable, Encodable {
    var card: [CardModel] = []
    
    init() {
        
    }
    
    required init?(map: Map) {
        card <- map["card"]

    }
    
    func mapping(map: Map) {
        
    }
    
}
