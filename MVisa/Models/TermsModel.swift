//
//  TermsModel.swift
//  MVisa
//
//  Created by Ali Merhie on 6/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class TermsModel: Mappable, Encodable {
    var text: String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        text <- map["text"]
        
    }
    
    func mapping(map: Map) {
        
    }
}
