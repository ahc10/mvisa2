//
//  PhoneBalanceModel.swift
//  MVisa
//
//  Created by Ali Merhie on 6/3/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class BalanceModel: Mappable, Encodable {
    var amount: Double?
    var currency: String?

    init() {
        
    }
    
    required init?(map: Map) {
        amount <- map["amount"]
        currency <- map["currency"]

    }
    
    func mapping(map: Map) {
        
    }
}

