//
//  ErrorModel.swift
//  CoronaCare
//
//  Created by Ali Merhie on 4/3/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class ErrorModel: Mappable {
    var error: String?
    var error_description: String?
    
    init() {
        
    }
    required init?(map: Map) {
        error <- map["title"]
        error_description <- map["message"]
    }
    
    func mapping(map: Map) {
        
    }
    
}
