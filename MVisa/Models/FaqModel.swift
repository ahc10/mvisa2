//
//  FaqModel.swift
//  MVisa
//
//  Created by Ali Merhie on 6/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class FaqModel: Mappable, Encodable {
    var question: String?
    var answer: String?

    init() {
        
    }
    
    required init?(map: Map) {
        question <- map["question"]
        answer <- map["answer"]

    }
    
    func mapping(map: Map) {
        
    }
}

class FaqsModel: Mappable, Encodable {
    var faqs: [FaqModel] = []

    init() {
        
    }
    
    required init?(map: Map) {
        faqs <- map["faqs"]

    }
    
    func mapping(map: Map) {
        
    }
}
