//
//  TimeModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/28/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class TimeModel: Mappable, Encodable {
    var remainingTime: Int?
    var remainingTimeOtp: Int?

    init() {
        
    }
    
    required init?(map: Map) {
        remainingTime <- map["remainingTime"]
      remainingTimeOtp <- map["remaining_time"]


        
    }
    
    func mapping(map: Map) {
        
    }
    
}
