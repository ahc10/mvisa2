//
//  QRData.swift
//  MVisa
//
//  Created by Ali Merhie on 6/22/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class QRData: Mappable, Encodable {
    var toAccountId: Int?
    var transferAmount: Double?
    var toClientId: Int?
    var currency: String?

    init() {
        
    }
    
    required init?(map: Map) {
        toAccountId <- map["toAccountId"]
        transferAmount <- map["transferAmount"]
        toClientId <- map["toClientId"]
        currency <- map["currency"]

    }
    
    func mapping(map: Map) {
        
    }
}
