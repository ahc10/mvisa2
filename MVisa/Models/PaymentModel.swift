//
//  PaymentModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/29/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentModel: Mappable, Encodable {
    var paymentId: String?
    var merchantName: String?
    var locationName: String?
    var amount: Double?
    var currency: String?
    var paymentType: Int?
    var date: String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        paymentId <- map["paymentId"]
        merchantName <- map["merchantName"]
        locationName <- map["locationName"]
        amount <- map["amount"]
        currency <- map["currency"]
        paymentType <- map["paymentType"]
        date <- map["createdDate"]
        
        
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
class PaymentsModel: Mappable, Encodable {
    var payments: [PaymentModel] = []
    
    init() {
        
    }
    
    required init?(map: Map) {
        payments <- map["payments"]
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
