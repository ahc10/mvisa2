//
//  ClaimStateModel.swift
//  MVisa
//
//  Created by Ali Merhie on 6/3/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

enum ClaimStatesEnum:  Int {
    case CantClaim = 0
    case CanClaim = 1
    case Claimed = 2
}

class ClaimStateModel: Mappable, Encodable {
    var card_info: Int?

    init() {
        
    }
    
    required init?(map: Map) {
        card_info <- map["card_info"]
    }
    
    func mapping(map: Map) {
        
    }
}

