//
//  UserModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/28/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModel: Mappable, Encodable {
    var userId: Int?
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var token: String?
    var refreshToken: String?
    var isVerified: Bool?

    init() {
        
    }
    
    required init?(map: Map) {
        userId <- map["userId"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        phoneNumber <- map["phoneNumber"]
        token <- map["token"]
        refreshToken <- map["refreshToken"]
        isVerified <- map["isVerified"]
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
