//
//  ResponseModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/28/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseModel<Element: Mappable>: Mappable {
    var status: String?
    var data: Element?

    init(){

    }
    required init?(map: Map) {
        status <- map["status"]
        data <- map["data"]
    }

    func mapping(map: Map) {

    }

}
