//
//  AppDelegate.swift
//  MVisa
//
//  Created by Ali Merhie on 5/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var bgTask : UIBackgroundTaskIdentifier!
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        //to receive notification token
        Messaging.messaging().delegate = self
        startLuanchScreen()
        
        return true
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Create a pseudo background task to system call applicationWillTerminate when app enter background
        // Default system will not call applicationWillTerminate when app enter background
        // applicationWillTerminate only called when user close app in app switcher or some special cases of system
        bgTask = application.beginBackgroundTask(expirationHandler: { () -> Void in
            application.endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskIdentifier.invalid
        })
        
        DispatchQueue.global(qos : .background).async() { () -> Void in
            self.bgTask = UIBackgroundTaskIdentifier.invalid
            application.endBackgroundTask(self.bgTask)
        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        Shared.logOut()
    }
    
    func startApp() {
        
        var vcIdentifier: ViewControllerModel = SignIn_VC //TabBar_VC
        
        if(isLoggedInDefualts){
            vcIdentifier = Main_VC
        }else{
            vcIdentifier = SignIn_VC
        }
        ScreenLoader().setRootNavigationController(viewController: vcIdentifier)
        //let nextViewController = storyboard.instantiateViewController(withIdentifier: TabBar_VC)
        //WindowHelper.shared.window.rootViewController = nextViewController
    }
    func startLuanchScreen(){
          ScreenLoader().setRootNavigationController(viewController: Luanch_VC)
    }
    
    // MARK: UISceneSession Lifecycle
    
    //    @available(iOS 13.0, *)
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    @available(iOS 13.0, *)
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MVisa")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

extension AppDelegate : UNUserNotificationCenterDelegate, MessagingDelegate {
    
    @available(iOS 10, *)
    // enable notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    //when app is in background or terminated
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // check for checkup notifications and cancel if already filled
        
        //               if response.actionIdentifier == CheckUp_VC{
        //                   if lastDateCheckupFilled == Date().toDateString(){
        //                       return
        //                   }
        //               }
        completionHandler()
    }
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // Perform background operation
        
        if let value = userInfo["actionId"] as? String {
            print(value) // output: "some-value"
        }
        if let actionId = userInfo["actionId"] as? String{
            
            if actionId == String(SilentPushActionEnum.Logout.rawValue) {
                if isLoggedInDefualts{
                    print("Silent Push: Logout")
                    Shared.expireSession()
                }
            }else if actionId == String(SilentPushActionEnum.RefreshMain.rawValue){
                if isLoggedInDefualts{
                    NotificationCenter.default.post(name: .updateMainPageData, object: nil)
                }
            }
        }
        // Inform the system after the background operation is completed.
        completionHandler(.newData)
    }
    
    // This function will be called right after user tap on the notification
    //        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //           let action = response.actionIdentifier
    //                  let request = response.notification.request
    //                let typeNotification = request.content.userInfo["typeNotification"]
    //            if let valueId = request.content.userInfo["valueId"] as? String {
    //               let vc = Shared.pushVC(rootView: UIApplication.getTopViewController() ?? UIViewController(), viewController: ReviewComments_VC) as! ReviewCommentsViewController
    //                vc.rateId = valueId
    //            }
    //
    //           completionHandler()
    //         }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        Shared.updateDeviceInfo()
        
        // NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
}

