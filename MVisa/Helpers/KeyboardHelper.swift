//
//  KeyBoardHelper.swift
//  CallerRate
//
//  Created by Ali Merhie on 3/29/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class KeyboardHelper {
    public static func dismiss() {
        UIApplication.shared.sendAction("resignFirstResponder", to:nil, from:nil, for:nil)
    }

}
