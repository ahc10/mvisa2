//
//  UserDefualtsHelper.swift
//  PalmReading
//
//  Created by Ali Merhie on 9/20/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import  ObjectMapper
import SwiftKeychainWrapper


var TokenKeyChain: String {
    set {
        KeychainWrapper.standard.set(newValue, forKey: "Token")
        ApiServices.reInitialize()
    }
    get {
        return KeychainWrapper.standard.string(forKey: "Token") ?? "NoToken"
    }
}

var RefreshTokenKeyChain: String {
    set {
        KeychainWrapper.standard.set(newValue, forKey: "RefreshTokenKeyChain")
        ApiServices.reInitialize()
    }
    get {
        return KeychainWrapper.standard.string(forKey: "RefreshTokenKeyChain") ?? "NoRefreshToken"
    }
}
var selectedLanguage: String? {
    set {
        UserDefaults.standard.set(newValue, forKey: "SelectedLanguage")
    }
    get {
        return UserDefaults.standard.string(forKey: "SelectedLanguage") ?? nil
    }
}
var FcmTokenDefualts: String? {
    set {
        UserDefaults.standard.set(newValue, forKey: "FcmTokenDefualts")
    }
    get {
        return UserDefaults.standard.string(forKey: "FcmTokenDefualts") ?? nil
    }
}


var isLoggedInDefualts: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "isLoggedInDefualts")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.bool(forKey: "isLoggedInDefualts") ?? false
    }
}
var isTermsAcceptedDefualts: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "isTermsAcceptedDefualts")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.bool(forKey: "isTermsAcceptedDefualts")
    }
}

var isLuanchedBeforeDefualts: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "isLuanchedBeforeDefualts")
    }
    get {
        return UserDefaults.standard.bool(forKey: "isLuanchedBeforeDefualts")
    }
}
var isContactsUploadedAfterLuanchDefualts: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "isContactsUpdatedAfterLuanch")
    }
    get {
        return UserDefaults.standard.bool(forKey: "isContactsUpdatedAfterLuanch") ?? false
    }
}

var UserInfoDefualts: UserModel {
    set {
        let userJson = try! JSONEncoder().encode(newValue)
        let jsonString = String(data: userJson, encoding: .utf8)!
        KeychainWrapper.standard.set(jsonString, forKey: "UserData")
    }
    get {
        let userJosn = KeychainWrapper.standard.string(forKey: "UserData") ?? ""
        let userData = Mapper<UserModel>().map(JSONString: userJosn) ?? UserModel()
        return userData
    }
}


//var UserAccountsDefualts: UserAccountsModel {
//    set {
//        let userJson = try! JSONEncoder().encode(newValue)
//        let jsonString = String(data: userJson, encoding: .utf8)!
//        UserDefaults.standard.set(jsonString, forKey: "UserAccounts")
//    }
//    get {
//        let userJosn = UserDefaults.standard.string(forKey: "UserAccounts") ?? ""
//        let userData = Mapper<UserAccountsModel>().map(JSONString: userJosn) ?? UserAccountsModel()
//        return userData
//    }
//}
//

