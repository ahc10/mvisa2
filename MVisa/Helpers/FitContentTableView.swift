//
//  FitContentTableView.swift
//  CoronaCare
//
//  Created by Ali Merhie on 4/6/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class FitContentTableView: UITableView {

    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }

}
