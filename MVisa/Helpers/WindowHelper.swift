//
//  WindowHelper.swift
//  SelfCare
//
//  Created by Ali Merhie on 10/30/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit

final class WindowHelper {
    public var window: UIWindow
    static let shared = WindowHelper()
    
    private init() {
        window = AppDelegate.shared.window!

//        window = UIWindow()
//        if #available(iOS 13.0, *) {
//            for scene in UIApplication.shared.connectedScenes {
//                if scene.activationState == .foregroundActive {
//                    window = ((scene as? UIWindowScene)!.delegate as! UIWindowSceneDelegate).window!!
//                    break
//                }
//            }
//        } else {
//            window = AppDelegate.shared.window!
//        }
        
    }
}
