
//
//  ViewCLickButtonHelper.swift
//  CoronaCare
//
//  Created by Ali Merhie on 4/20/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit

class ViewClickButtonHelper: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = MyTapGestureRecognizer(target: self , action: #selector(self.selectTexField))
        tap.numberOfTapsRequired = 1
        
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }
    
    @objc func selectTexField(_ sender: MyTapGestureRecognizer) {
        for case let button as UIButton in self.subviews {
            button.sendActions(for: .touchUpInside)
        }
        
    }
    
}

