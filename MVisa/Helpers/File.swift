//
//  File.swift
//  CallerRate
//
//  Created by Ali Merhie on 12/31/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

//import UIKit
//import SkeletonView
//
// // MARK: - UITableViewDataSource
// extension SkeletonCollectionDataSource: UITableViewDataSource {
//     func numberOfSections(in tableView: UITableView) -> Int {
//         return originalTableViewDataSource?.numSections(in: tableView) ?? 0
//     }
//     
//     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//         return originalTableViewDataSource?.collectionSkeletonView(tableView, numberOfRowsInSection: section) ?? 0
//     }
//     
//     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//         let cellIdentifier = originalTableViewDataSource?.collectionSkeletonView(tableView, cellIdentifierForRowAt: indexPath) ?? ""
//         let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
//         skeletonCellIfContainerSkeletonIsActive(container: tableView, cell: cell)
//         return cell
//     }
// }
