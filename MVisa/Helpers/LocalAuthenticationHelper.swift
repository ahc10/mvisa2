//
//  DeviceAuthenticationHelper.swift
//  MVisa
//
//  Created by Ali Merhie on 6/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit
//import LocalAuthentication
import BiometricAuthentication

class LocalAuthenticationHelper: NSObject {
    var completion: ((_ isSuccess: Bool) -> ())? = nil
    //let localAuthenticationContext = LAContext()
    
    init(completion : @escaping (Bool)->Void ) {
        super.init()
        self.completion = completion
        //authenticationWithTouchID()
        showPasscodeAuthentication(message: "Access Card Info")
    }
    
}

extension LocalAuthenticationHelper {
    
    func authenticationWithTouchID() {
        
        // Set AllowableReuseDuration in seconds to bypass the authentication when user has just unlocked the device with biometric
        BioMetricAuthenticator.shared.allowableReuseDuration = 30
        
        // start authentication
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "") { [self] (result) in
            
            switch result {
            case .success( _):
                
                // authentication successful
                //self?.showLoginSucessAlert()
                self.completion!(true)
                
            case .failure(let error):
                
                switch error {
                    
                // device does not support biometric (face id or touch id) authentication
                case .passcodeNotSet:
                    self.completion!(true)
                    
                case .biometryNotAvailable:
                    //self?.showErrorAlert(message: error.message())
                    print("biometryNotAvailable")
                    self.showPasscodeAuthentication(message: error.message())
                    
                // No biometry enrolled in this device, ask user to register fingerprint or face
                case .biometryNotEnrolled:
                    //self?.showGotoSettingsAlert(message: error.message())
                    print("biometryNotEnrolled")
                    self.showPasscodeAuthentication(message: error.message())
                    
                // show alternatives on fallback button clicked
                case .fallback:
                    //self?.txtUsername.becomeFirstResponder() // enter username password manually
                    print("fallback")
                    self.showPasscodeAuthentication(message: error.message())
                    
                    // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                case .biometryLockedout:
                    print("biometryLockedout")
                    self.showPasscodeAuthentication(message: error.message())
                    
                // do nothing on canceled by system or user
                case .canceledBySystem, .canceledByUser:
                    break
                    
                // show error for any other reason
                default:
                    self.showErrorAlert(message: error.message())
                }
            }
        }
    }
    
    // show passcode authentication
    func showPasscodeAuthentication(message: String) {
        BioMetricAuthenticator.shared.allowableReuseDuration = 30
        BioMetricAuthenticator.authenticateWithPasscode(reason: message) { [self] (result) in
            switch result {
            case .success( _):
                self.completion!(true) // passcode authentication success
            case .failure(let error):
                switch error {
                case .passcodeNotSet:
                    self.completion!(true)
                default:
                    print(error.message())
                }
                
            }
        }
    }
}
// MARK: - Alerts
extension LocalAuthenticationHelper {
    
    func showAlert(title: String, message: String) {
        Shared.displayAlert(title: title, message: message, cancelText: "Dismiss")
    }
    
    func showLoginSucessAlert() {
        //showAlert(title: "Success", message: "Login successful")
        self.completion!(true)
    }
    
    func showErrorAlert(message: String) {
        showAlert(title: "Error", message: message)
    }
    
    func showGotoSettingsAlert(message: String) {
        Shared.displayConfirmAlert(title: "Go to settings", message: message, confirmText: "Go to settings", cancelText: "Cancel") { (isConfirmed) in
            if isConfirmed{
                // open settings
                let url = URL(string: UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:])
                }
            }
        }
        
    }
}
