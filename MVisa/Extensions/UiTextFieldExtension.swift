//
//  UiTextFieldExtension.swift
//  CallerRate
//
//  Created by Ali Merhie on 11/26/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setPlaceHolderColor(color: UIColor){
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : color])
      }
//    @IBInspectable var placeholderColor: UIColor {
//        get {
//            guard let currentAttributedPlaceholderColor = attributedPlaceholder?.attribute(NSAttributedString.Key.foregroundColor, at: 0, effectiveRange: nil) as? UIColor else { return UIColor.clear }
//            return currentAttributedPlaceholderColor
//        }
//        set {
//            guard let currentAttributedString = attributedPlaceholder else { return }
//            let attributes = [NSAttributedString.Key.foregroundColor : newValue]
//
//            attributedPlaceholder = NSAttributedString(string: currentAttributedString.string, attributes: attributes)
//        }
//    }
}
