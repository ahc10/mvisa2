//
//  DateHelper.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/20/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
extension String {
    
     func toDateUTC () -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
       return dateFormatter.date(from: self + " 12:00:00 +0000")!
    }
    
    func toDateUTCComplete () -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        return dateFormatter.date(from: self )!
    }
    
    func toTimeUTC () -> Date {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        return timeFormatter.date(from: "2019/01/01 " + self)!
    }
    
//    func toDateFormatted() -> Date {
////          let formatter = DateFormatter()
////        formatter.timeZone = TimeZone.current
////        formatter.dateFormat = "yyyy-MM-dd"
////        return formatter.date(from: self ?? "") ?? Date()
//
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        formatter.calendar = Calendar(identifier: .iso8601)
//        formatter.locale = Locale(identifier: "en_US_POSIX")
//        formatter.timeZone = TimeZone(secondsFromGMT: 0)
//      return formatter.date(from: self ?? "") ?? Date()
//    }
    func toDateFormatted() -> Date {
          let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: self ?? "") ?? Date()
    }
    func UTCToLocal() -> String {
        let calendar = Calendar.current

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: self) ?? Date()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm aa"
        return dateFormatter.string(from: dt)

//        //check how old is the date and view string as full date or past hours
//        let components = calendar.dateComponents([.day], from: dt ?? Date(), to: Date())
//        if (components.day ?? 0) >= 1 {
//        return dateFormatter.string(from: dt!)
//        }
//        return dt?.timeAgo() ?? ""
    }
}

extension Date {
    
    func toDateString() -> String {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        return dateStringFormatter.string(from: self)
    }

    
    func toTimeString() -> String {
        let timeStringFormatter = DateFormatter()
        timeStringFormatter.dateFormat = "h:mm a"
        return timeStringFormatter.string(from: self)
        
    }
    func toTime24String() -> String {
        let timeStringFormatter = DateFormatter()
        timeStringFormatter.dateFormat = "hh:mm:ss"
        return timeStringFormatter.string(from: self)
        
    }
    func timeAgo() -> String {
         let formatter = DateComponentsFormatter()
         formatter.unitsStyle = .full
         formatter.allowedUnits = [.year, .month, .day, .hour, .minute, .second] //[.year, .month, .day, .hour, .minute, .second]
         formatter.zeroFormattingBehavior = .dropAll
         formatter.maximumUnitCount = 1
        return String(format: formatter.string(from: self, to: Date()) ?? "", locale: .current) + " ago"
     }
}
