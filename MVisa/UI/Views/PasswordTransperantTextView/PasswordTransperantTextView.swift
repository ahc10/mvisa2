//
//  PasswordTextView.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class PasswordTransperantTextView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var hideButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    var isHiddenText: Bool = true {
        didSet{
            textField.isSecureTextEntry = isHiddenText
            hideButton.tintColor = isHiddenText ? UIColor.fadedButtonColor : UIColor.kehle
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    init() {
        super.init(frame: .infinite)
        setUp()
    }
    private func setUp(){
        Bundle.main.loadNibNamed("PasswordTransperantTextView", owner: self, options: nil)

        hideButton.setTintedImage(named: "eye", state: .normal)
        isHiddenText = true
        self.addSubview(view)
        addConstraints()
        
    }

    @IBAction func hideClicked(_ sender: Any) {
        isHiddenText = !isHiddenText
    }
    
    func addConstraints() {
        view.translatesAutoresizingMaskIntoConstraints = false
        var anchors = [NSLayoutConstraint]()
        anchors.append(topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        anchors.append(bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        anchors.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        anchors.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        NSLayoutConstraint.activate(anchors)
        
    }

}
