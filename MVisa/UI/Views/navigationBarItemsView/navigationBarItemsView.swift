//
//  navigationBarView.swift
//  MVisa
//
//  Created by Ali Merhie on 5/7/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class navigationBarItemsView: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var billButton: UIButton!
    @IBOutlet weak var dotsButton: UIButton!
    @IBInspectable var isLight: Bool = false{
        didSet {
            updateIsLight()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    init() {
        super.init(frame: .infinite)
        setUp()
    }
    
    private func setUp(){
        Bundle.main.loadNibNamed("navigationBarItemsView", owner: self, options: nil)

        self.addSubview(view)
        addConstraints()
    }
    private func updateIsLight(){
        if isLight{
            billButton.setTintedImage(named: "notificationBell", state: .normal)
            billButton.tintColor = .white
            
            dotsButton.setTintedImage(named: "moreDots", state: .normal)
            dotsButton.tintColor = .white
        }
    }
    @IBAction func moreClicked(_ sender: Any) {
        ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: More_VC)
    }
    func addConstraints() {
        view.translatesAutoresizingMaskIntoConstraints = false
        var anchors = [NSLayoutConstraint]()
        anchors.append(topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        anchors.append(bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        anchors.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        anchors.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        NSLayoutConstraint.activate(anchors)
        
    }
}
