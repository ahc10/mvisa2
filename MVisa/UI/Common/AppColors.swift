//
//  AppColors.swift
//  MVisa
//
//  Created by Ali Merhie on 5/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

extension UIColor {
    
    open class var primaryColor: UIColor {
        
        return   UIColor(named: "primaryColor")!
    }
    open class var textFadedColor: UIColor {
        
        return   UIColor(named: "textFadedColor")!
    }
    open class var textMainColor: UIColor {
        
        return   UIColor(named: "textMainColor")!
    }
    open class var gradientFirstColor: UIColor {
        
        return   UIColor(named: "gradientFirstColor")!
    }
    open class var gradientSecondColor: UIColor {
        
        return   UIColor(named: "gradientSecondColor")!
    }
    open class var fadedButtonColor: UIColor {
        
        return   UIColor(named: "fadedButtonColor")!
    }
    open class var kehle: UIColor {
        
        return   UIColor(named: "kehle")!
    }
    open class var kehleLight: UIColor {
        
        return   UIColor(named: "kehleLight")!
    }
    
}

