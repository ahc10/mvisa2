//
//  ScreenLoader.swift
//  MVisa
//
//  Created by Ali Merhie on 5/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit
import SemiModalViewController

class ScreenLoader  {
    
    public func setRootNavigationController(viewController: ViewControllerModel){
        let storyboard = UIStoryboard(name: viewController.storyBoardIdentifier, bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: viewController.viewControllerIdentifier)
        //add setting button to navigation
        //        if viewController != Login_VC && viewController != Register_VC && viewController != Verify_VC {
        //            let buttonSetting = UIBarButtonItem(image: UIImage(named: "settings"), style: .plain, target: self, action: #selector(self.openSetting))
        //            buttonSetting.tintColor = UIColor.white
        //            nextViewController.navigationItem.rightBarButtonItem  = buttonSetting
        //        }
        
        let nav = RTLNavigationController(rootViewController: nextViewController)
        
        WindowHelper.shared.window.rootViewController = nav
    }
    
    public func pushVC(rootView: UIViewController, viewController: ViewControllerModel) -> UIViewController{
        let storyboard = UIStoryboard(name: viewController.storyBoardIdentifier, bundle: nil)
        let nextViewController = (storyboard.instantiateViewController(withIdentifier: viewController.viewControllerIdentifier))
        rootView.navigationController?.pushViewController(nextViewController, animated: true)
        
        return nextViewController
        
    }
    public func popUpView(mainView: UIViewController, popUpVc: ViewControllerModel) -> UIViewController{
        let storyboard = UIStoryboard(name: popUpVc.storyBoardIdentifier, bundle: nil)
        let presentedViewController = storyboard.instantiateViewController(withIdentifier: popUpVc.viewControllerIdentifier)
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        //presentedViewController.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        mainView.present(presentedViewController, animated: true, completion: nil)

        return presentedViewController
    }
    public func popUpHalfView(mainView: UIViewController, popUpVc: ViewControllerModel, height: CGFloat = UIScreen.main.bounds.height/1.5) -> UIViewController{
        let storyboard = UIStoryboard(name: popUpVc.storyBoardIdentifier, bundle: nil)
        let presentedViewController = storyboard.instantiateViewController(withIdentifier: popUpVc.viewControllerIdentifier)
        let options = [
            SemiModalOption.pushParentBack: false,
        ]
        //presentedViewController.view.backgroundColor = UIColor.red
        presentedViewController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height)
        
        mainView.presentSemiViewController(presentedViewController, options: options, completion: {
            print("Completed!")
        }, dismissBlock: {
            print("Dismissed!")
        })
        return presentedViewController
    }
}
