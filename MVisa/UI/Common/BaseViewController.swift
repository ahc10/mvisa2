//
//  BaseViewController.swift
//  CoronaCare
//
//  Created by Ali Merhie on 4/3/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var isLightStatus = false
    deinit {
        print("Memory to be released soon ")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(setUpPage), name: .internetConnectionBack, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAppDidBecomeActiveNotification(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = isLightStatus ? .lightContent : .darkContent
        } else {
            // Fallback on earlier versions
        }
    }
    @objc public func setUpPage(showLoader: Bool = true){
        
    }
    @objc func handleAppDidBecomeActiveNotification(notification: Notification) {
        print("did become active")
        setUpPage(showLoader: false)
    }
    @objc func willEnterForeground() {
        print("will enter foreground")
    }
    //    @objc func reloadPage(_ notification:Notification) {
    //        self.viewDidLoad()
    //    }
    public  func startSkeletonAnimation(view: UIView ){
        //let gradient = SkeletonGradient(baseColor: UIColor(named: "charcoalGrey")!)
        view.showAnimatedGradientSkeleton()
    }
    public  func hideSkeletonAnimation(view: UIView){
        view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.5))
    }
}
