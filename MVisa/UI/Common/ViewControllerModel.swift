//
//  ViewControllerModel.swift
//  MVisa
//
//  Created by Ali Merhie on 5/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation

class ViewControllerModel  {
    var viewControllerIdentifier: String!
    var storyBoardIdentifier: String!
    
    init(viewControllerIdentifier: String, storyBoardIdentifier: String ) {
        self.viewControllerIdentifier = viewControllerIdentifier
        self.storyBoardIdentifier = storyBoardIdentifier
    }
    
}
