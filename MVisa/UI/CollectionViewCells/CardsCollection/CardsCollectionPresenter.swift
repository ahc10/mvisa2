//
//  AddCollectionViewCell.swift
//  SelfCare
//
//  Created by Ali Merhie on 10/31/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//
import UIKit
import MSPeekCollectionViewDelegateImplementation

protocol CardsCollectionDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func currentPageChanged(index: Int)
    func didSelectItem(item: CardModel)
}
class CardsCollectionPresenter: NSObject {
    private let cellIdentinfier = "CardsCollectionViewCell"
    public weak var delegate: CardsCollectionDelegate?
    private let collectionView: UICollectionView
    private var currentPage = 0 {
        didSet {
            //if currentPage != oldValue && currentPage < items.count{
            if currentPage != oldValue {
            print(currentPage)
            delegate?.currentPageChanged(index: currentPage)
            }
        }
    }
    var items = [CardModel]() {
        didSet {
            //collectionView.configureForPeekingBehavior(behavior: behavior)
            collectionView.reloadData()
        }
    }
    
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        
        super.init()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        self.collectionView.frame = collectionView.frame.insetBy(dx: -50.0, dy: 0.0)
        collectionView.contentInset.left = 20
        collectionView.register(UINib.init(nibName: cellIdentinfier, bundle: nil), forCellWithReuseIdentifier: cellIdentinfier)
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.scrollDirection = .horizontal
        }
    }
    
    private var collectionViewCurrentPage: Int {
        let pageWidth = collectionView.frame.width
        var currentPage = Float(collectionView.contentOffset.x / pageWidth)
        let itemsCount = items.count
        
        if fmodf(currentPage, 1) != 0 {
            currentPage = currentPage + 1
        }
        
        if itemsCount > 0 && Int(currentPage) >= itemsCount {
            return itemsCount - 1
        }
        
//        if currentPage > Float(self.currentPage){
//            return Int(currentPage.rounded(.awayFromZero))
//        }else{
//            return Int(currentPage.rounded(.towardZero))
//
//        }
        return Int(currentPage)
    }
    
    @objc func scrollToNextPage() {
        let itemsCount = self.items.count
        self.currentPage += 1
        
        if itemsCount > 0 && self.currentPage >= itemsCount {
            self.currentPage = 0
        }
        let indexPath = IndexPath(row: self.currentPage, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
    }
    public func reCentralizeCells(){
        self.collectionView.scrollToItem(at: IndexPath(row: currentPage, section: 0), at: .centeredHorizontally, animated: true)

    }
}

extension CardsCollectionPresenter: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentinfier, for: indexPath) as! CardsCollectionViewCell
         let item = items[indexPath.row]
         cell.setUpCell(item: item)
        
        return cell
    }
}

extension CardsCollectionPresenter: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentPage = collectionViewCurrentPage
    }
    //center cells
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        currentPage = collectionViewCurrentPage
        
//        if scrollView == self.collectionView {
//
//        self.collectionView.scrollToItem(at: IndexPath(item: currentPage, section: 0), at: .centeredHorizontally, animated: true)
//        }
        
        if scrollView == self.collectionView {
            var currentCellOffset = self.collectionView.contentOffset
            currentCellOffset.x += self.collectionView.frame.width / 2
            if let indexPath = self.collectionView.indexPathForItem(at: currentCellOffset) {
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        currentPage = collectionViewCurrentPage
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        delegate?.didSelectItem(item: item)
    }
    
    // func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    //        behavior.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    //    }
    
    
}

extension CardsCollectionPresenter: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return collectionView.bounds.size
        return CGSize(width: collectionView.bounds.width - 40, height: collectionView.bounds.height)
    }
    
}

