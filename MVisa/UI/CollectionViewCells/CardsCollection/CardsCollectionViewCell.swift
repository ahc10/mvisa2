//
//  CardsCollectionViewCell.swift
//  MVisa
//
//  Created by Ali Merhie on 5/5/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class CardsCollectionViewCell: BaseCollectionViewCell {
    
    @IBOutlet weak var bulletsLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var holderNameLabel: UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    public func setUpCell(item: CardModel) {
        bulletsLabel.text = "\u{2022}\u{2022}\u{2022}\u{2022} \u{2022}\u{2022}\u{2022}\u{2022} \u{2022}\u{2022}\u{2022}\u{2022}"
        titleLabel.text = "Virtual Credit Card"
        cardNumberLabel.text = "\(item.last4Digits ?? "")"
        holderNameLabel.text = item.holderName
        expiryLabel.text = item.expiryDate
    }
}
