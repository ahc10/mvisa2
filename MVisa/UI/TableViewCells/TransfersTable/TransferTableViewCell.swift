//
//  PaymentsTableViewCell.swift
//  MVisa
//
//  Created by Ali Merhie on 5/6/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class TransferTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    public func setUpCell(item: TransferModel) {
        titleLabel.text = (item.isScratchCard ?? false) ? item.scratchCard : item.msisdn
        iconImageView.image = UIImage(named: (item.isScratchCard ?? false) ? "screatchPayment" : "dollar")
        priceLabel.text = "+ \(item.amount ?? 0) \(item.currency ?? "")"
        timeLabel.text = item.createdDate?.UTCToLocal()
    }


}
