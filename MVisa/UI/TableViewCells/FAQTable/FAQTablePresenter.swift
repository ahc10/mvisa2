//
//  FAQTablePresenter.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import SkeletonView

class FAQTablePresenter: NSObject {
    private let cellIdentinfier = "FAQTableViewCell"
    private let tableView: UITableView
    
    var items = [FaqModel]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var didSelectItem: ((_ item: FaqModel) -> ())?
    
    init(tableView: UITableView) {
        self.tableView = tableView
        
        super.init()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.register(UINib(nibName: cellIdentinfier, bundle: nil), forCellReuseIdentifier: cellIdentinfier)
    }
}

extension FAQTablePresenter: SkeletonTableViewDataSource, UITableViewDelegate {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        skeletonView.register(UINib(nibName: cellIdentinfier, bundle: nil), forCellReuseIdentifier: cellIdentinfier)
        
        return cellIdentinfier
    }
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        didSelectItem?(item)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let  cell = tableView.dequeueReusableCell(withIdentifier: cellIdentinfier) as! FAQTableViewCell
         cell.setUpCell(item: item)
        //cell.hideSkeleton()
        
        return cell
    }
}

