//
//  FAQTableViewCell.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    public func setUpCell(item: FaqModel) {
        questionLabel.text = item.question
    }


}
