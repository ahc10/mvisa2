//
//  PaymentsTablePresenter.swift
//  MVisa
//
//  Created by Ali Merhie on 5/6/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView
import EmptyDataSet_Swift

class PaymentsTablePresenter: NSObject {
    private let cellIdentinfier = "PaymentsTableViewCell"
    private let tableView: UITableView
    var haveMoreButton = true

    var items = [PaymentModel]() {
        didSet {
            tableView.reloadData()
            tableView.reloadEmptyDataSet()

        }
    }
    
    var didSelectItem: ((_ item: PaymentModel) -> ())?
    
    init(tableView: UITableView, haveMoreButton: Bool = true) {
        self.tableView = tableView
        self.haveMoreButton = haveMoreButton

        super.init()
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset.top = 10
        tableView.contentInset.bottom = 20

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.register(UINib(nibName: cellIdentinfier, bundle: nil), forCellReuseIdentifier: cellIdentinfier)
        tableView.register(UINib(nibName: "MoreButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreButtonTableViewCell")

    }
    
}

extension PaymentsTablePresenter: SkeletonTableViewDataSource, UITableViewDelegate {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        skeletonView.register(UINib(nibName: cellIdentinfier, bundle: nil), forCellReuseIdentifier: cellIdentinfier)
        
        return cellIdentinfier
    }
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //            let review = reviewsDisplayed[indexPath.row]
        //            let vc =  Shared.pushVC(rootView: UIApplication.getTopViewController()!, viewController: ContactProfile_VC) as! ContactProfileViewController
        //            vc.userId = review.userId
        //            vc.isEditOpen = true
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return  haveMoreButton ? 2 : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var itemsCount = items.count
        if section == 0 {
            return itemsCount
        }else{
            return itemsCount == 0 ? 0 : 1
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        didSelectItem?(item)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let item = items[indexPath.row]
            let  cell = tableView.dequeueReusableCell(withIdentifier: cellIdentinfier) as! PaymentsTableViewCell
            cell.hideSkeleton()
            cell.setUpCell(item: item)
            return cell
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "MoreButtonTableViewCell") as! MoreButtonTableViewCell
            cell.delegate = self
            return cell
        }
    }
}
extension PaymentsTablePresenter: MoreButtonTableViewCellDelegate{
    func viewMoreClicked() {
        let vc = ScreenLoader().pushVC(rootView: UIApplication.getTopViewController()!, viewController: PaymentTransfer_VC)
    }
}
extension PaymentsTablePresenter: EmptyDataSetSource, EmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let attributes = [NSAttributedString.Key.font: UIFont(name: "FiraSans-Light", size: 15)!]

        return NSAttributedString(string: "You Do Not Have Any Payments", attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "noDataMagnifier")
    }
    
}
