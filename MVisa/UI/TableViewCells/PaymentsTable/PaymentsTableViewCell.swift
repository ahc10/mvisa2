//
//  PaymentsTableViewCell.swift
//  MVisa
//
//  Created by Ali Merhie on 5/6/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class PaymentsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    public func setUpCell(item: PaymentModel) {
        let isFees = (item.paymentType == 1 || item.paymentType == 2)
        iconImageView.image = UIImage(named: isFees ? "montyPayment" : "card")
        titleLabel.text = item.merchantName
        locationLabel.text = item.locationName
        timeLabel.text = item.date?.UTCToLocal()
        priceLabel.text = "- \(item.amount ?? 0) \(item.currency ?? "")"
    }


}
