//
//  MoreButtonTableViewCell.swift
//  MVisa
//
//  Created by Ali Merhie on 5/6/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

protocol MoreButtonTableViewCellDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func viewMoreClicked()
}
class MoreButtonTableViewCell: UITableViewCell {
    
    public weak var delegate: MoreButtonTableViewCellDelegate?
    @IBOutlet weak var moreButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        moreButton.setTitle("View More", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func moreClicked(_ sender: Any) {
        delegate?.viewMoreClicked()
    }
}
