//
//  VerifyViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/15/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class VerifyViewController: BaseViewController {
    
    @IBOutlet weak var holderView: GradientView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var codeView: KWVerificationCodeView!
    var timer: Timer?
    var totalTime = 60
    var code: String?
    
    override func viewDidDisappear(_ animated: Bool) {
          timer?.invalidate()
      }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }

     override func handleAppDidBecomeActiveNotification(notification: Notification) {
         super.handleAppDidBecomeActiveNotification(notification: notification)
         updateTimeRemaining()
     }
    override func viewDidLoad() {
        super.viewDidLoad()
        arrowImageView.loadGif(name: "downArrow")
        titleLabel.text = "Almost There !"
        subtitleLabel.text = "Verify your number to start"
        descLabel.text = "Please enter the verification code sent to your phone number"
        verifyButton.setTitle("Verify", for: .normal)
        resendLabel.text = ""
        resendButton.setTitle("Resend", for: .normal)
        
        codeView.keyboardType = .numberPad
        codeView.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        arrowImageView.isUserInteractionEnabled = true
        arrowImageView.addGestureRecognizer(tapGestureRecognizer)
        
        updateTimeRemaining()
        
        // Do any additional setup after loading the view.
    }
    func updateTimeRemaining() {
        if let timer = self.timer {
                   timer.invalidate()
                   self.timer = nil
               }
        ApiServices.shared.getResendTimeRemaining { (result, statusCode) in
            self.totalTime = result.remainingTime ?? 0
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
            
        }
        
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.dismiss(animated: true) {
            
        }
    }
    @objc func updateTimer() {
        print(self.totalTime)
        self.resendButton.setTitle("Resend Code in \(self.timeFormatted(self.totalTime))" , for: .normal)
        // will show timer
        if totalTime != 0 {
            self.resendButton.isEnabled = false
            totalTime -= 1  // decrease counter timer
        } else {
            self.resendButton.setTitle("Resend", for: .normal)
            if let timer = self.timer {
                self.resendButton.isEnabled = true
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    @IBAction func verifyClicked(_ sender: Any) {
        self.showHud()
        ApiServices.shared.verify(userId: UserInfoDefualts.userId ?? 0, otpCode: self.code ?? "") { (result, statusCode) in
            self.hideHUD()
            if statusCode == 200 {
                Shared.logIn(userData: result)
            }
        }
    }
    @IBAction func resendClicked(_ sender: Any) {
        self.showHud()
        ApiServices.shared.resendOtp { (statusCode) in
            self.hideHUD()
            self.updateTimeRemaining()
        }
    }
}
extension VerifyViewController: KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
        let code = codeView.getVerificationCode().trimmingCharacters(in: .whitespaces)
        let count = code.count
        self.code = ""
        if count == codeView.digits {
            self.code = code
        }
    }
}
