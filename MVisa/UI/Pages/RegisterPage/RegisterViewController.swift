//
//  RegisterViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/5/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import CoreTelephony

class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var holderView: GradientView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var passwordTextView: PasswordTextView!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var haveAccountLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    let networkInfo = CTTelephonyNetworkInfo()
    
    var phoneNumber: String?
    var phoneCode: String?
    var countryCode: String?
    var isValidPhoneNumber: Bool = false
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        arrowImageView.loadGif(name: "downArrow")
        titleLabel.text = "Welcome on board !"
        subtitleLabel.text = "Your journey starts now !"
        firstNameTextField.placeholder = "First name"
        firstNameTextField.setPlaceHolderColor(color: UIColor(named: "WhiteFaded")!)
        lastNameTextField.placeholder = "Last name"
        lastNameTextField.setPlaceHolderColor(color: UIColor(named: "WhiteFaded")!)
        passwordTextView.textField.placeholder = "Password"
        passwordTextView.textField.setPlaceHolderColor(color: UIColor(named: "WhiteFaded")!)
        registerButton.setTitle("Register", for: .normal)
        haveAccountLabel.text = "have an account? "
        loginButton.setUnderlinedText(title: "Login now")
        phoneNumberTextField.delegate = self
        phoneNumberTextField.setPlaceHolderColor(color: UIColor(named: "WhiteFaded")!)
        phoneNumberTextField.hasPhoneNumberExample = true
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.textColor = UIColor.white
        
//        phoneNumberTextField.textContentType = .username
//        if #available(iOS 12.0, *) {
//            passwordTextView.textField.textContentType = .newPassword
//        } else {
//            // Fallback on earlier versions
//        }
        
        var countryCode = "US"
        //get country from operator
        if let carrier = networkInfo.subscriberCellularProvider {
            countryCode = carrier.isoCountryCode ?? "US"
        }else{
            countryCode = Locale.current.regionCode ?? "US"
        }
        phoneNumberTextField.setFlag(countryCode: FPNCountryCode(rawValue: countryCode.uppercased())!)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        arrowImageView.isUserInteractionEnabled = true
        arrowImageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func registerClicked(_ sender: Any) {
        let firstName = firstNameTextField.text ?? ""
        let lastName = lastNameTextField.text ?? ""
        let password = passwordTextView.textField.text ?? ""
        let phone = phoneNumber ?? ""
        let code = phoneCode ?? ""
        
        if firstName == "" || lastName == "" || password == "" {
            Shared.displayAlert(title: "Missing Fields", message: "All fields are required !", cancelText: "Done")
            
        }else if !isValidPhoneNumber {
            Shared.displayAlert(title: "Invalid Number", message: "Please insert a valid phone number", cancelText: "Done")
            
        }else{
            self.showHud()
            ApiServices.shared.signUp(firstName: firstName, lastName: lastName, phoneCode: code , phoneNumber: phone, password: password) { (result, statusCode) in
                self.hideHUD()
                if statusCode == 200 {
                    Shared.updateUserInfo(userData: result)
                    ScreenLoader().popUpView(mainView: self, popUpVc: Verify_VC)
                }
            }
        }
    }
    @IBAction func backLoginClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    
}
extension RegisterViewController: FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        
    }
    
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
        phoneCode = dialCode
        countryCode = code
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        phoneNumber = ""
        isValidPhoneNumber = isValid
        if isValid {
            // Do something...
            phoneNumber =  (phoneNumberTextField.getRawPhoneNumber() ?? "").replacingOccurrences(of: " ", with: "")
            print(phoneNumber)
            // Output "+33600000001"
            // phoneNumberTextField.getFormattedPhoneNumber(format: .International)  // Output "+33 6 00 00 00 01"
            //phoneNumberTextField.getFormattedPhoneNumber(format: .National)       // Output "06 00 00 00 01"
            // phoneNumberTextField.getFormattedPhoneNumber(format: .RFC3966)        // Output "tel:+33-6-00-00-00-01"
            //phoneNumberTextField.getRawPhoneNumber()
            //print(phoneNumberTextField)// Output "600000001"
        } else {
            // Do something...
        }
    }
}
