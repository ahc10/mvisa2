//
//  CardExplainViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/7/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class CardExplainViewController: BaseViewController {

    @IBOutlet weak var arrowsImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrowsImageView.loadGif(name: "arrows")
        descLabel.text = "Click the card to flip it and reveal \n all information "
        closeButton.setUnderlinedText(title: "Close")
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        isLuanchedBeforeDefualts = true
        self.dismiss(animated: true) {
        }
    }
}
