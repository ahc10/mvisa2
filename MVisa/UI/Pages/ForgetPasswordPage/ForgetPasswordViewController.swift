//
//  ForgetPasswordViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 6/5/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

enum ForgetPassDisplayEnum: Int {
    case number = 0
    case otp = 1
    case pass = 2
}
class ForgetPasswordViewController: BaseViewController {
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var stepsButtonView: UIView!
    @IBOutlet weak var numberHolderView: UIView!
    @IBOutlet weak var otpHolderView: UIView!
    @IBOutlet weak var resendHolderView: UIView!
    @IBOutlet weak var passwordHolderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var mainButton: GradientButton!
    @IBOutlet weak var stage1Button: GradientButton!
    @IBOutlet weak var stage2Button: GradientButton!
    @IBOutlet weak var codeView: KWVerificationCodeView!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var passTextView: PasswordTextView!
    @IBOutlet weak var confirmPassTextView: PasswordTextView!
    var phoneNumber: String?
    var timer: Timer?
    var totalTime = 60
    var code: String?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    override func handleAppDidBecomeActiveNotification(notification: Notification) {
        super.handleAppDidBecomeActiveNotification(notification: notification)
        updateTimeRemaining()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Reset Password"
        cancelButton.setTitle("Cancel", for: .normal)
        phoneNumberTextField.placeholder = "Enter Phone number"
        resendButton.setTitle("Resend", for: .normal)
        resendLabel.text = "Didn't Receive a code?"
        passTextView.textField.placeholder = "Enter new Password"
        confirmPassTextView.textField.placeholder = "Confirm new Password"
        codeView.keyboardType = .numberPad
        codeView.delegate = self
        phoneNumberTextField.becomeFirstResponder()
        
        changePageDisplay(type: .number)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
    }
    
    func updateTimeRemaining() {
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
        ApiServices.shared.getForgetPassResendTimeRemaining(phoneNumber: phoneNumber ?? "") { (result, statusCode) in
            if statusCode == 200 {
                self.totalTime = result.remainingTimeOtp ?? 0
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
                
            }
        }
    }
    
    @objc func updateTimer() {
        print(self.totalTime)
        self.resendButton.setTitle("Resend Code in \(self.timeFormatted(self.totalTime))" , for: .normal)
        // will show timer
        if totalTime != 0 {
            self.resendButton.isEnabled = false
            totalTime -= 1  // decrease counter timer
        } else {
            self.resendButton.setTitle("Resend", for: .normal)
            if let timer = self.timer {
                self.resendButton.isEnabled = true
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    private func changePageDisplay(type: ForgetPassDisplayEnum){
        mainButton.tag = type.rawValue
        if type == .number{
            descLabel.text = "To Reset password an OTP will be sent to your phone number"
            stage1Button.setTitle("1", for: .normal)
            stage1Button.startColor = .gradientFirstColor
            stage1Button.endColor = .gradientSecondColor
            stage1Button.setTitleColor(.white, for: .normal)
            stage1Button.setImage(UIImage(), for: .normal)
            stage1Button.isEnabled = false
            
            stage2Button.setTitle("2", for: .normal)
            stage2Button.startColor = .fadedButtonColor
            stage2Button.endColor = .fadedButtonColor
            stage2Button.setTitleColor(.textFadedColor, for: .normal)
            stage2Button.isEnabled = false
            
            stepsButtonView.isHidden = false
            numberHolderView.isHidden = false
            otpHolderView.isHidden = true
            resendHolderView.isHidden = true
            passwordHolderView.isHidden = true
            
            mainButton.setTitle("Proceed", for: .normal)
        }else if type == .otp{
            descLabel.text = "Please enter the OTP to proceed"
            stage1Button.setTitle("", for: .normal)
            stage1Button.setImage(UIImage(named: "penIcon"), for: .normal)
            stage1Button.startColor = .gradientSecondColor
            stage1Button.endColor = .gradientSecondColor
            stage1Button.isEnabled = true
            
            stage2Button.setTitle("2", for: .normal)
            stage2Button.startColor = .gradientFirstColor
            stage2Button.endColor = .gradientSecondColor
            stage2Button.setTitleColor(.white, for: .normal)
            stage2Button.isEnabled = false
            
            stepsButtonView.isHidden = false
            numberHolderView.isHidden = true
            otpHolderView.isHidden = false
            resendHolderView.isHidden = false
            passwordHolderView.isHidden = true
            
            mainButton.setTitle("Proceed", for: .normal)
            updateTimeRemaining()
        }else if type == .pass{
            descLabel.text = ""
            
            stepsButtonView.isHidden = true
            numberHolderView.isHidden = true
            otpHolderView.isHidden = true
            resendHolderView.isHidden = true
            passwordHolderView.isHidden = false
            mainButton.setTitle("Confirm", for: .normal)
            
        }
    }
    @IBAction func mainClicked(_ sender: UIButton) {
        if sender.tag == ForgetPassDisplayEnum.number.rawValue{
            phoneMainClicked()
        }else if sender.tag == ForgetPassDisplayEnum.otp.rawValue{
            otpMainClicked()
        }else if sender.tag == ForgetPassDisplayEnum.pass.rawValue{
            passMainClicked()
        }
    }
    private func phoneMainClicked(){
        let number = phoneNumberTextField.text ?? ""
        if number == ""{
            Shared.displayAlert(title: "Required Field", message: "Phone Number is required!", cancelText: "Dismiss")
        }else{
            phoneNumber = phoneNumberTextField.text ?? ""
            self.showHud()
            ApiServices.shared.forgetPaasword(phoneNumber: phoneNumber ?? "") {  (statusCode) in
                self.hideHUD()
                if statusCode == 200 {
                    self.changePageDisplay(type: .otp)
                }
            }
        }
        
    }
    private func otpMainClicked(){
        if self.code == nil || self.code == ""{
            Shared.displayAlert(title: "Missing Field", message: "Please fill the otp!", cancelText: "Dismiss")
        }else{
            self.showHud()
            ApiServices.shared.verifyForgetPaasword(phoneNumber: phoneNumber ?? "", code: code ?? "") { (statusCode) in
                self.hideHUD()
                if statusCode == 200 {
                    self.changePageDisplay(type: .pass)
                }
            }
        }
    }
    private func passMainClicked(){
        let pass = passTextView.textField.text ?? ""
        let confirmPass = confirmPassTextView.textField.text ?? ""
        
        if pass == "" || confirmPass == "" {
            Shared.displayAlert(title: "Missing Field", message: "new password and confirm new password are both required!", cancelText: "Dismiss")
        }else if pass != confirmPass {
            Shared.displayAlert(title: "Nott Matching", message: "new password and confirm password didn't match!", cancelText: "Dismiss")
        }else{
            self.showHud()
            ApiServices.shared.resetPassword(phoneNumber: phoneNumber ?? "", code: code ?? "", newPass: pass, confirmPass: confirmPass) { (statusCode) in
                self.hideHUD()
                if statusCode == 200 {
                    self.closePage()
                    Shared.showToast(text: "Password Changed Successfully")
                }
            }
        }
    }
    @IBAction func stage1Clicked(_ sender: Any) {
        changePageDisplay(type: .number)
    }
    @IBAction func resendClicked(_ sender: Any) {
        self.showHud()
        ApiServices.shared.resendForgetPassOtp(phoneNumber: phoneNumber ?? "") { (statusCode) in
            self.hideHUD()
            self.updateTimeRemaining()
            
        }
    }
    @objc private func closePage(){
        self.dismiss(animated: true) {}
    }
    @IBAction func cancelClicked(_ sender: Any) {
        closePage()
    }
}

extension ForgetPasswordViewController: KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
        let code = codeView.getVerificationCode().trimmingCharacters(in: .whitespaces)
        let count = code.count
        self.code = ""
        if count == codeView.digits {
            self.code = code
        }
    }
}
