//
//  TopUpSuccessViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class TopUpSuccessViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var closeView: UIView!
    var isScratch = false
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tickImageView.loadGif(name: "tick")
        doneButton.setTitle("Done", for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
        
    }
    
    public func setText(title: String, description: String){
        titleLabel.text = title
        descLabel.text = description
    }
    
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func doneClicked(_ sender: Any) {
        closePage()
    }
}
