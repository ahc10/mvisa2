//
//  ChooseDateViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/7/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

protocol ChooseDateDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func dateSelected(fromDate: String, toDate: String)
}
class ChooseDateViewController: BaseViewController {
    
    public weak var delegate: ChooseDateDelegate?
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var fromDateButton: UIButton!
    @IBOutlet weak var toDateButton: UIButton!
    @IBOutlet weak var confirmButton: GradientButton!
    @IBOutlet weak var titleLabel: UILabel!
    var selectedButton: UIButton?
    var fromDate: String?{
        didSet{
            //make the maximum date todate ot today
            updateCalendaerPickerLimits(isFrom: true)
            
            if let from = fromDate {
                fromDateButton.setTitle(from, for: .normal)
            }
            checkIfFilled()
        }
    }
    var toDate: String?{
        didSet{
            //set the minumum date from date or past
            updateCalendaerPickerLimits(isFrom: false)

            if let to = toDate {
                toDateButton.setTitle(to, for: .normal)
                setpickerDate(date: toDate)
            }
            checkIfFilled()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.roundCorners(corners: [.topLeft, .topRight], radius: 20.0)
        titleLabel.text = "Choose Date" //"chooseDate".localized()
        fromDateButton.setTitle("From Date", for: .normal)
        toDateButton.setTitle("To Date", for: .normal)
        confirmButton.setTitle("Confirm", for: .normal)
        datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        changeButtonState(isEnabled: false)
        initializePage()
    }
    @IBAction func buttonClicked(_ sender: UIButton) {
        changeButtonState(button: selectedButton, isSelected: false)
        selectedButton = sender
        changeButtonState(button: selectedButton, isSelected: true)
        //check the selected date and set on datepicker
        if sender.tag == 0{
            setpickerDate(date: fromDate)
            updateCalendaerPickerLimits(isFrom: true)

        }else{
            guard toDate != nil else {
                toDate = Date().toDateString()
                return
            }
            setpickerDate(date: toDate)
            updateCalendaerPickerLimits(isFrom: false)

        }
    }
    @IBAction func confirmClicked(_ sender: Any) {
        self.dismissSemiModalView()
        delegate?.dateSelected(fromDate: fromDate!, toDate: toDate!)
    }
    private func updateCalendaerPickerLimits(isFrom: Bool){
        if isFrom {
            datePicker.maximumDate = (toDate ?? Date().toDateString()).toDateFormatted()
            datePicker.minimumDate = "1900-01-01".toDateFormatted()
        }else{
            datePicker.maximumDate = Date()
            datePicker.minimumDate = (fromDate ?? "1900-01-01").toDateFormatted()
        }
    }
    private func changeButtonState(button: UIButton?, isSelected: Bool){
        if let btn = button{
            let color = isSelected ? UIColor.primaryColor : UIColor.textFadedColor
            btn.borderColor = color
            btn.setTitleColor(color, for: .normal)
        }
        
    }
    public func initializePage(){
        //initialize selected button
        selectedButton = fromDateButton
        changeButtonState(button: selectedButton, isSelected: true)
        guard fromDate != nil else {
            fromDate = Date().toDateString()
            return
        }
    }
    private func changeButtonState(isEnabled: Bool){
        confirmButton.isEnabled = isEnabled
        confirmButton.setTitleColor(isEnabled ? UIColor.white : UIColor.black, for: .normal)
        if isEnabled {
            confirmButton.startColor = UIColor.gradientSecondColor
            confirmButton.endColor = UIColor.gradientFirstColor
        }else{
            confirmButton.backgroundColor = UIColor.fadedButtonColor
        }
    }
    private func getDate(_ datePicker: UIDatePicker) -> String{
        print(datePicker.date)
        
        let date = datePicker.date.toDateString()
        return date
    }
    private func setpickerDate(date: String?){
        datePicker.setDate(date?.toDateFormatted() ?? Date(), animated: true)
    }
    
    @objc func datePickerChanged(picker: UIDatePicker) {
        if let button = selectedButton {
            let selectedDate = getDate(picker)
            button.setTitle(selectedDate, for: .normal)
            if button.tag == 0 {
                fromDate = selectedDate
            }else{
                toDate = selectedDate
            }
        }
    }
    
    private func checkIfFilled(){
        if fromDate != nil && toDate != nil {
            changeButtonState(isEnabled: true)
        }
    }
}
