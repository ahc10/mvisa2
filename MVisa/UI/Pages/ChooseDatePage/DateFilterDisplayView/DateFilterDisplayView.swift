//
//  DateFilterDisplayView.swift
//  MRewards
//
//  Created by Ali Merhie on 2/24/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

protocol DateFilterDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func dateSelected(fromDate: String, toDate: String)
}
class DateFilterDisplayView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    public weak var delegate: DateFilterDelegate?
    var fromDate: String?
    var toDate: String?
    var isSingleDate = true
    var singleDate = Date()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    init() {
        super.init(frame: .infinite)
        setUp()
    }
    
    private func setUp(){
        loadViewFromNib()
        //Bundle.main.loadNibNamed("DateFilterDisplayView", owner: self, options: nil)
        self.addSubview(view)
        addConstraints()
        resetButton.setTitle("Reset", for: .normal)
        decreaseButton.setTintedImage(named: "leftRound", state: .normal)
        increaseButton.setTintedImage(named: "rightRound", state: .normal)
        ChangeButtonState(button: decreaseButton, isEnabled: true)
        changeDateBox(isOneDay: true)
        setSingleDate(date: singleDate)
        
    }
    public func displayDateSelectionPage(){
        let vc = ScreenLoader().popUpHalfView(mainView: UIApplication.getTopViewController()!, popUpVc: ChooseDate_VC) as! ChooseDateViewController
         vc.delegate = self
         vc.fromDate = fromDate
         vc.toDate = toDate
         vc.initializePage()
    }
    
    private func changeDateBox(isOneDay: Bool){
        decreaseButton.isHidden = !isOneDay
        increaseButton.isHidden = !isOneDay
        resetButton.isHidden = isOneDay
        isSingleDate = isOneDay
    }
    public func setSingleDate(date: Date){
        let dateString = date.toDateString()
        changeDateBox(isOneDay: true)

        self.delegate?.dateSelected(fromDate: dateString, toDate: dateString)
        self.fromDate = nil
        self.toDate = nil
        singleDate = date
        if Calendar.current.isDate(date, inSameDayAs:Date()) {
            dateLabel.text = "Today"
            ChangeButtonState(button: increaseButton, isEnabled: false)
        }else{
            dateLabel.text = date.toDateString()
            ChangeButtonState(button: increaseButton, isEnabled: true)
        }
    }
    private func ChangeButtonState(button: UIButton, isEnabled: Bool){
        button.isEnabled = isEnabled
        button.tintColor = isEnabled ? UIColor.kehle : UIColor.kehle.withAlphaComponent(0.9)
    }
    @IBAction func resetClicked(_ sender: Any) {
        changeDateBox(isOneDay: true)
        setSingleDate(date: Date())
        fromDate = nil
        toDate = nil
    }
    @IBAction func increaseDateClicked(_ sender: Any) {
        singleDate = Calendar.current.date(byAdding: .day, value: 1, to: singleDate)!
        setSingleDate(date: singleDate)
    }
    @IBAction func decreaseDateClicked(_ sender: Any) {
        singleDate = Calendar.current.date(byAdding: .day, value: -1, to: singleDate)!
        
        setSingleDate(date: singleDate)
        
    }
    func addConstraints() {
        view.translatesAutoresizingMaskIntoConstraints = false
        var anchors = [NSLayoutConstraint]()
        anchors.append(topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        anchors.append(bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        anchors.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        anchors.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        NSLayoutConstraint.activate(anchors)
    }
    
}
extension DateFilterDisplayView: ChooseDateDelegate {
    func dateSelected(fromDate: String, toDate: String) {
        if fromDate == toDate {
            self.setSingleDate(date: fromDate.toDateFormatted())
            return
        }
        self.delegate?.dateSelected(fromDate: fromDate, toDate: toDate)
        self.fromDate = fromDate
        self.toDate = toDate
        changeDateBox(isOneDay: false)
        let string = NSMutableAttributedString(string: " From \(fromDate) To \(toDate)")
        string.setFontForText(String(fromDate), with: UIFont(name: "FiraSans-Bold", size: 17.0)!)
        string.setFontForText(String(toDate), with: UIFont(name: "FiraSans-Bold", size: 17.0)!)
        self.dateLabel.attributedText = string
    }
}
