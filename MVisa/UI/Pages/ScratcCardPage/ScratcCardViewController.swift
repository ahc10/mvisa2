//
//  ScratcCardViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class ScratcCardViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var confirmButton: GradientButton!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var scratchImageView: UIImageView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var textTitleLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmButton.changePrimaryButtonState(isEnabled: true)
        scratchImageView.isHidden = false
        scratchImageView.loadGif(name: "scratchCard")
        perform(#selector(hideScratch), with: nil, afterDelay: 2)
        titleLabel.text = "Scratch Card"
        cancelButton.setTitle("Cancel", for: .normal)
        descLabel.text = "Enter the number revealed in the scratch card"
        textTitleLabel.text = "Enter Number"
        numberTextField.placeholder = "Card Number"
        confirmButton.setTitle("Confirm", for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }
    @objc private func hideScratch(){
        scratchImageView.isHidden = true
    }
    
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func confirmClicked(_ sender: Any) {
        let cardNumber = numberTextField.text ?? ""
        if cardNumber == ""{
            Shared.displayAlert(title: "Missing Field", message: "Card Number is required!", cancelText: "Dismiss")
        }else{
            self.showHud()
            ApiServices.shared.topUpScratchCard(scratchCard: cardNumber) { (result, statusCode) in
                self.hideHUD()
                if statusCode == 200 {
                    self.dismiss(animated: true) {
                        NotificationCenter.default.post(name: .updateMainPageData, object: nil)
                        let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: TopUpSuccess_VC) as! TopUpSuccessViewController
                        vc.setText(title: "Scratch Card", description: "\(result.amount ?? 0) \(result.currency ?? "") have been successfully Topped Up to your balance")
                    }
                }
            }
        }
        
    }
    @IBAction func cancelClicked(_ sender: Any) {
        closePage()
    }
    
}
