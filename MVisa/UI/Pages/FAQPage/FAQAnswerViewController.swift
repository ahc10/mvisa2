//
//  FAQAnswerViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class FAQAnswerViewController: BaseViewController {

    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "FAQ"
        backButton.setTitle("Back", for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
                  self.closeView.addGestureRecognizer(gesture)
        
    }

    public func setBody(question: String, answer: String){
        questionLabel.text = question
        answerLabel.text = answer
    }
    @objc private func closePage(){
        self.dismiss(animated: true) {}
    }

    @IBAction func backClicked(_ sender: Any) {
        closePage()
    }
}
