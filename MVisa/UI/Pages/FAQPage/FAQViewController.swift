//
//  FAQViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class FAQViewController: BaseViewController {

    @IBOutlet weak var topView: GradientView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchtextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var faqTablePresenter: FAQTablePresenter?
    var faqs: [FaqModel] = []
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        searchtextField.addTarget(self, action: #selector(searchFieldDidChange(_:)), for: UIControl.Event.editingChanged)

        isLightStatus = true
        titleLabel.text = "FAQ"
        searchtextField.placeholder = "Search"
        faqTablePresenter = FAQTablePresenter(tableView: tableView)
        faqTablePresenter?.items = []
        faqTablePresenter?.didSelectItem = didSelectFAQ(item:)
        setUpPage()
    }
    override func setUpPage(showLoader: Bool = true) {
        super.setUpPage(showLoader: showLoader)
        self.showHud()
        ApiServices.shared.getFAQs { (result, statusCode) in
            self.hideHUD()
            if statusCode == 200 {
                self.faqs = result.faqs
                self.faqTablePresenter?.items = self.faqs
            }
        }
    }
    @objc func searchFieldDidChange(_ textField: UITextField) {
        if textField.text == "" {
            self.faqTablePresenter?.items = faqs
        }else{
            self.faqTablePresenter?.items = faqs.filter  { (($0.question?.lowercased().contains(textField.text?.lowercased() ?? ""))!) ||  (($0.answer?.contains(textField.text ?? ""))!)}
        }
        
    }
    private func didSelectFAQ(item: FaqModel){
        let vc = ScreenLoader().popUpView(mainView: self, popUpVc: FAQAnswer_VC) as! FAQAnswerViewController
        vc.setBody(question: item.question ?? "", answer: item.answer ?? "")
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
