//
//  AlertViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/15/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

protocol AlertConfirmDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func optionSlected(isConfirmed: Bool)
}
class AlertViewController: BaseViewController {
    public var delegate: AlertConfirmDelegate?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.loadGif(name: "question")
        // Do any additional setup after loading the view.
    }
    @IBAction func confirmClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.optionSlected(isConfirmed: true)
            
        }
    }
    
    @IBAction func rejectClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.optionSlected(isConfirmed: false)
        }
        
    }
    
    
}
