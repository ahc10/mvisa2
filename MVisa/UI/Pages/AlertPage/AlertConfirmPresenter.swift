//
//  AlertConfirmPresenter.swift
//  MVisa
//
//  Created by Ali Merhie on 5/15/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit

class AlertConfirmPresenter: NSObject {
    var completion: ((_ isConfirmed: Bool) -> ())? = nil
    
    init( description: String, note: String = "", confirmTitle: String, rejectTitle: String, completion : @escaping (Bool)->Void ) {
        super.init()
        self.completion = completion
        let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: AlertConfirm_VC) as! AlertViewController
        vc.delegate = self
        vc.textLabel.text = description
        vc.noteLabel.text = note
        vc.acceptButton.setTitle(confirmTitle, for: .normal)
        vc.rejectButton.setTitle(rejectTitle, for: .normal)
    }
    
}
extension AlertConfirmPresenter: AlertConfirmDelegate {
    func optionSlected(isConfirmed: Bool) {
        self.completion!(isConfirmed)
        
    }
    
    
}
