//
//  MoreViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController {
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var profileView: HighlightView!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var helpView: HighlightView!
    @IBOutlet weak var settingsView: HighlightView!
    @IBOutlet weak var faqLabel: UILabel!
    @IBOutlet weak var faqView: HighlightView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var termsView: HighlightView!
    @IBOutlet weak var logoutLabel: UILabel!
    @IBOutlet weak var logoutView: HighlightView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "More"
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        versionLabel.text = "Virtual Credit Card V\(appVersion!)"
        profileLabel.text = "My Profile"
        profileView.addTapGesture { [weak self] in
            ScreenLoader().popUpView(mainView: self!, popUpVc: MyProfile_VC)
        }
        
        helpLabel.text = "Help"
        helpView.addTapGesture { [weak self] in
            ScreenLoader().popUpView(mainView: self!, popUpVc: Help_VC)
        }
        
        settingsLabel.text = "Notification Settings"
        settingsView.addTapGesture { [weak self] in
        }
        
        faqLabel.text = "FAQ"
        faqView.addTapGesture { [weak self] in
            self?.dismiss(animated: false) {
                ScreenLoader().pushVC(rootView: UIApplication.getTopViewController()!, viewController: FAQ_VC)
            }
        }
        
        termsLabel.text = "Terms & Conditions"
        termsView.addTapGesture {[weak self] in
            self?.dismiss(animated: false) {
                ScreenLoader().pushVC(rootView: UIApplication.getTopViewController()!, viewController: Terms_VC)
            }
        }
        logoutLabel.text = "Log out"
        logoutView.addTapGesture { [weak self] in
            Shared.displayConfirmAlert(title: "Log out", message: "Are you sure you want to log out ?", confirmText: "Logout", cancelText: "Cancel") { (isConfirmed) in
                if isConfirmed{
                    Shared.logOut()
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var settingsLabel: UILabel!
    
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    
    
}
