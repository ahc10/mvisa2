//
//  AirtimeTransferViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class AirtimeTransferViewController: BaseViewController {

    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var numberTitleLabel: UILabel!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var numberDescLabel: UILabel!
    @IBOutlet weak var amountTitleLAbel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var confirmButton: GradientButton!
    @IBOutlet weak var holderView: UIView!
    var amount = "" {
        didSet{
            if amount == "" {
                confirmButton.changePrimaryButtonState(isEnabled: false)
            }else{
                confirmButton.changePrimaryButtonState(isEnabled: true)

            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amountTextField.addTarget(self, action: #selector(amountTextChanged), for: UIControl.Event.editingChanged)
            
        titleLabel.text = "Transfer"
        cancelButton.setTitle("Cancel", for: .normal)
        descLabel.text = "Enter the amount to complete the transfer"
        numberTitleLabel.text = "MobileNumber"
        numberTextField.isEnabled = false
        numberDescLabel.text = "* Transfers are made from your mobile number only"
        amountTitleLAbel.text = "Amount"
        amountTextField.placeholder = "Amount"
        confirmButton.setTitle("Confirm", for: .normal)
        
        numberTextField.text = UserInfoDefualts.phoneNumber
        balanceLabel.text = "Airtime Balance 0.00 USD"
        amount = ""
        
        startSkeletonAnimation(view: self.balanceLabel)
        ApiServices.shared.getPhoneBalance { (result, statusCode) in
            self.hideSkeletonAnimation(view: self.balanceLabel)
            if statusCode == 200{
                self.balanceLabel.text = "Airtime Balance \(result.amount ?? 0) \(result.currency ?? "")"

            }
        }

    }
    @objc func amountTextChanged(_ textField: UITextField) {
        amount = textField.text ?? ""
    }
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func confirmClicked(_ sender: Any) {
        self.showHud()
        ApiServices.shared.topUpAirtime(amount: Double(amount) ?? 0) { (result, statusCode) in
            self.hideHUD()
            if statusCode == 200 {
                NotificationCenter.default.post(name: .updateMainPageData, object: nil)
                self.dismiss(animated: true) {
                     let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: TopUpSuccess_VC) as! TopUpSuccessViewController
                     vc.setText(title: "Transfer", description: "\(result.amount ?? 0) \(result.currency ?? "") have been successfully Topped Up to your balance")
                     
                 }
            }
        }

    }
    @IBAction func cancelClicked(_ sender: Any) {
        closePage()
    }
}
