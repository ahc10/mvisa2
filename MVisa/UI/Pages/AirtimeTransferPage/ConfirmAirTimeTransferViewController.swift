
//
//  ConfirmAirTimeTransferViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 6/29/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class ConfirmAirTimeTransferViewController: UIViewController {
    
    @IBOutlet weak var continureButton: GradientButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Transfer"
        cancelButton.setTitle("Cancel", for: .normal)
        continureButton.setTitle("Continue to Airtime Transfer", for: .normal)
        descLabel.text = "Transfers done via airtime will be deducted from your mobile credit balance"
        // Do any additional setup after loading the view.
    }
    
    
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        closePage()
    }
    @IBAction func continueClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: AirtimeTransfer_VC)
            
        }
    }
}
