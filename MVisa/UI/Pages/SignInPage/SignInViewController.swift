//
//  SignInViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import CoreTelephony

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgetButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var noAccountLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    let networkInfo = CTTelephonyNetworkInfo()
    var phoneNumber: String?
    var phoneCode: String?
    var countryCode: String?
    var isValidPhoneNumber: Bool = false
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Welcome"
        subtitleLabel.text = "Login to continue your journey"
        passwordTextField.placeholder = "Password"
        forgetButton.setUnderlinedText(title: "Forgot Password?")
        signInButton.setTitle("Login", for: .normal)
        noAccountLabel.text = "Don't have an account? "
        registerButton.setUnderlinedText(title: "Register now")
        phoneNumberTextField.delegate = self
        phoneNumberTextField.hasPhoneNumberExample = true
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.textColor = UIColor.white
        phoneNumberTextField.setPlaceHolderColor(color: UIColor(named: "WhiteFaded")!)
        passwordTextField.setPlaceHolderColor(color: UIColor(named: "WhiteFaded")!)
//        phoneNumberTextField.textContentType = .username
//        passwordTextField.textContentType = .password
        var countryCode = "US"
        //get country from operator
        if let carrier = networkInfo.subscriberCellularProvider {
            countryCode = carrier.isoCountryCode ?? "US"
        }else{
            countryCode = Locale.current.regionCode ?? "US"
        }
        phoneNumberTextField.setFlag(countryCode: FPNCountryCode(rawValue: countryCode.uppercased())!)
    }
    
    @IBAction func forgotClicked(_ sender: Any) {
        ScreenLoader().popUpView(mainView: self, popUpVc: ForgetPassword_VC)
    }
    @IBAction func signInClicked(_ sender: Any) {
        let totalNumber = "\(phoneCode ?? "")\(phoneNumber ?? "")"
        let password = passwordTextField.text ?? ""
        
        if totalNumber == "" || password == "" {
            
            Shared.displayAlert(title: "Missing Fields", message: "All fields are required !", cancelText: "Done")
            
        }else if !isValidPhoneNumber {
            Shared.displayAlert(title: "Invalid Number", message: "Please insert a valid phone number", cancelText: "Done")
            
        }else{
            self.showHud()
            ApiServices.shared.login(phoneNumber: totalNumber, password: password) { (result, statusCode) in
                self.hideHUD()
                if statusCode == 200 {
                    let userData = result
                    Shared.updateUserInfo(userData: userData)
                    if userData.isVerified ?? false{
                        Shared.logIn(userData: userData)
                    }else{
                        ScreenLoader().popUpView(mainView: self, popUpVc: Verify_VC)
                    }
                    
                }
            }
        }
    }
    @IBAction func registerClicked(_ sender: Any) {
        ScreenLoader().popUpView(mainView: self, popUpVc: Register_VC)
    }
}
extension SignInViewController: FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
        phoneCode = dialCode
        countryCode = code
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        phoneNumber = ""
        isValidPhoneNumber = isValid
        if isValid {
            // Do something...
            phoneNumber =  (phoneNumberTextField.getRawPhoneNumber() ?? "").replacingOccurrences(of: " ", with: "")
            print(phoneNumber)
            // Output "+33600000001"
            // phoneNumberTextField.getFormattedPhoneNumber(format: .International)  // Output "+33 6 00 00 00 01"
            //phoneNumberTextField.getFormattedPhoneNumber(format: .National)       // Output "06 00 00 00 01"
            // phoneNumberTextField.getFormattedPhoneNumber(format: .RFC3966)        // Output "tel:+33-6-00-00-00-01"
            //phoneNumberTextField.getRawPhoneNumber()
            //print(phoneNumberTextField)// Output "600000001"
        } else {
            // Do something...
        }
    }
}
