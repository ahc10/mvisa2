//
//  QRScanViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import  ObjectMapper
import Lottie

class QRScanViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var animtionView: UIView!
    @IBOutlet weak var scannerView: QRScannerView!{
        didSet {
            scannerView.delegate = self
        }
    }
    
    var qrData: QRData? = nil {
        didSet {
            print(qrData)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.setTitle("Cancel", for: .normal)
        titleLabel.text = "Scan QR code"
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
//        let animation = AnimationView(name: "barcode-scanner")
//              animtionView.addSubview(animation)
//              animation.contentMode = .scaleAspectFit
//              animtionView.contentMode = .scaleAspectFit
//              animation.frame = animtionView.bounds
//              animation.animationSpeed = 1
//              animation.loopMode = .autoReverse
//              animation.play(toProgress: 1){ (finished) in
//                  /// Animation stopped
//              }
    }
    
    
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func closeClicked(_ sender: Any) {
        closePage()
    }
}
extension QRScanViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        //let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        //scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        Shared.displayAlert(title: "Error", message: "Scanning Failed. Please try again", cancelText: "Dismiss")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        //self.qrData = QRData(codeString: str)
        self.dismiss(animated: true) {
            
            if let data = Mapper<QRData>().map(JSONString: str ?? "") {
                let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: Pay_VC) as! PayViewController
                vc.setupPage(title: "Payment Information", paymentData: data, type: .button)
            }else{
                Shared.displayAlert(title: "Invalid", message: "Invalid QR Code!", cancelText: "Dismiss")
            }
        }
    }
}
