//
//  MainViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/5/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import LocalAuthentication

enum CardsPageTabs: Int {
    case None = -1
    case CardInfo = 0
    case Payments = 1
    case Trasnfers = 2
}
class MainViewController: BaseViewController {
    
    let myContext = LAContext()
    let myLocalizedReasonString = "Biometric Authntication testing !! "
    
    var isFirstAppear = true
    @IBOutlet var infoImages: [UIImageView]?
    @IBOutlet var infoLabels: [UILabel]?
    @IBOutlet var infoTabViews: [UIView]?
    
    @IBOutlet weak var freezeImageView: UIImageView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var noCardsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cardsCollectionView: UICollectionView!
    @IBOutlet weak var cardTabButtton: GradientButton!
    @IBOutlet weak var paymentsTabButton: GradientButton!
    @IBOutlet weak var transfersTabButton: GradientButton!
    @IBOutlet weak var cardInforHolderView: UIView!
    @IBOutlet weak var paymentsHolderView: UIView!
    @IBOutlet weak var trasnfersHolderView: UIView!
    @IBOutlet weak var paymentTitleLabel: UILabel!
    @IBOutlet weak var paymentsTableView: UITableView!
    @IBOutlet weak var transfersTitleLabel: UILabel!
    @IBOutlet weak var transferTableView: UITableView!
    @IBOutlet weak var cardBalanceTitleLabel: UILabel!
    @IBOutlet weak var cardBalanceValueLabel: UILabel!
    @IBOutlet weak var dueAmountTitleLabel: UILabel!
    @IBOutlet weak var dueAmountLabel: UILabel!
    @IBOutlet weak var outstandingAmountTitleLabel: UILabel!
    @IBOutlet weak var outstandingAmountLabel: UILabel!
    @IBOutlet weak var topupLabel: UILabel!
    @IBOutlet weak var topupView: HighlightView!
    @IBOutlet weak var payLabel: UILabel!
    @IBOutlet weak var payView: HighlightView!
    @IBOutlet weak var freezeLabel: UILabel!
    @IBOutlet weak var freezeView: HighlightView!
    @IBOutlet weak var deactivateLabel: UILabel!
    @IBOutlet weak var deactivateView: HighlightView!
    @IBOutlet weak var dueDateTitleLabel: UILabel!
    @IBOutlet weak var dueDateValueLabel: UILabel!
    @IBOutlet weak var subscribtionAmountTitleLabel: UILabel!
    @IBOutlet weak var subscribtionAmountValueLabel: UILabel!
    @IBOutlet weak var subscribtionDateTitleLabel: UILabel!
    @IBOutlet weak var subscribtionDateValueLabel: UILabel!
    private var cardsCollectionPresenter: CardsCollectionPresenter?
    private var paymentsTablePresenter: PaymentsTablePresenter?
    private var transferTablePresenter: TransferTablePresenter?
    var cards: [CardModel] = []{
        didSet{
            cardsCollectionPresenter?.items = cards
            if cards.count > 0 {
                selectedCard = cards[0]
            }
        }
    }
    var selectedCard: CardModel? {
        didSet{
            selectedPayments = selectedCard?.payments ?? []
            selectedTransfers = selectedCard?.transfers ?? []
            cardBalanceValueLabel.text = "\(selectedCard?.cardBalance ?? 0) \(selectedCard?.currency ?? "")"
            dueAmountLabel.text = "\(selectedCard?.dueAmount ?? 0) \(selectedCard?.currency ?? "")"
            outstandingAmountLabel.text = "\(selectedCard?.outstandingAmount ?? 0) \(selectedCard?.currency ?? "")"
            dueDateValueLabel.text = selectedCard?.dueDate ?? ""
            subscribtionAmountValueLabel.text = "\(selectedCard?.subscriptionDueAmount ?? 0)"
            subscribtionDateValueLabel.text = "\(selectedCard?.subscriptionDueDate ?? "")"
            setupFreezeButton(isFreezed: selectedCard?.isFreezed ?? false)
        }
    }
    var selectedPayments: [PaymentModel] = []{
        didSet{
            paymentsTablePresenter?.items = selectedPayments
            
        }
    }
    var selectedTransfers: [TransferModel] = []{
        didSet{
            transferTablePresenter?.items = selectedTransfers
        }
    }
    
    private var currentPage: Int = 0 {
        didSet{
            
        }
    }
    private var currentTabSelected: Int = 0{
        didSet{
            changeSelectedTab(selectedTab: currentTabSelected)
        }
    }
    override func setUpPage(showLoader: Bool = true) {
        if showLoader{
            showHud()
        }
        ApiServices.shared.getCards { (result, statusCode) in
            self.hideHUD()
            if statusCode == 200{
                self.cards = result.card
                let have = self.cards.count != 0
                self.changePageDisplay(HaveCards: have)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        cardsCollectionPresenter?.reCentralizeCells()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFirstAppear{
            setUpPage(showLoader: false)
        }
        isFirstAppear = false
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Shared.updateDeviceInfo()
        NotificationCenter.default.addObserver(self, selector: #selector(setUpPage), name: .updateMainPageData, object: nil)
        changePageDisplay(HaveCards: false)
        cardsCollectionPresenter = CardsCollectionPresenter(collectionView: cardsCollectionView)
        cardsCollectionPresenter?.delegate = self
        //cardsCollectionPresenter?.didSelectItem = {[weak self] in  self?.didSelectCard(item:)}()
        cardsCollectionPresenter?.items = []
        
        paymentsTablePresenter = PaymentsTablePresenter(tableView: paymentsTableView)
        paymentsTablePresenter?.items = []
        
        transferTablePresenter = TransferTablePresenter(tableView: transferTableView)
        transferTablePresenter?.items = []
        
        paymentsHolderView.roundCorners(corners: [.topLeft,.topRight], radius: 30)
        cardTabButtton.tag = CardsPageTabs.CardInfo.rawValue
        paymentsTabButton.tag = CardsPageTabs.Payments.rawValue
        transfersTabButton.tag = CardsPageTabs.Trasnfers.rawValue
        noDataLabel.text = "No Data To Display"
        titleLabel.text = "Virtual Credit Card"
        cardTabButtton.setTitle("Card Info", for: .normal)
        paymentsTabButton.setTitle("Payments", for: .normal)
        transfersTabButton.setTitle("Top Up", for: .normal)
        paymentTitleLabel.text = "Recent Payments"
        transfersTitleLabel.text = "Recent Top Up"
        cardBalanceTitleLabel.text = "Card Balance"
        dueAmountTitleLabel.text = "Due Amount"
        outstandingAmountTitleLabel.text = "Outstanding Amount"
        subscribtionDateTitleLabel.text = "Subscribtion Due Date"
        subscribtionAmountTitleLabel.text = "Subscribtion Due Amount"
        self.titleLabel.adjustsFontSizeToFitWidth = true
        cardBalanceValueLabel.text = "0.0 USD"
        dueAmountLabel.text = "0.00 USD"
        outstandingAmountLabel.text = "0.00 USD"
        dueDateTitleLabel.text = "Due Date"
        dueDateValueLabel.text = ""
        
        if !isLuanchedBeforeDefualts {
            ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: CardExplain_VC)
        }
        
        ApiServices.shared.getClaimState { (result, statusCode) in
            if statusCode == 200 {
                let state = result.card_info
                if state == ClaimStatesEnum.CanClaim.rawValue {
                    let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: ClaimCard_VC) as! ClaimCardViewController
                    vc.canClaim = true
                    vc.delegate = self
                    
                }else if state == ClaimStatesEnum.CantClaim.rawValue{
                    let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: ClaimCard_VC) as! ClaimCardViewController
                    vc.canClaim = false
                }
            }
        }
        
        setUpPage()
        
        topupLabel.text = "Top Up"
        topupView.addTapGesture { [weak self] in
            ScreenLoader().popUpView(mainView: self!, popUpVc: TopupOptions_VC)
        }
        
        payLabel.text = "Scan To Pay"
        payView.addTapGesture { [weak self] in
            // ScreenLoader().popUpView(mainView: self!, popUpVc: PayOptions_VC)
            ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc:QRScan_VC)
            
        }
        
        freezeLabel.text = "Freeze Card"
        
        
        deactivateLabel.text = "Deactivate Card"
        deactivateView.addTapGesture { [weak self] in
            AlertConfirmPresenter(description: "Are you sure you want to Deactivate your card ?", note: "*Please note that upon deactivating, your card will be removed from Virtual Credit Card  ", confirmTitle: "Yes", rejectTitle: "Cancel") { (isConfirmed) in
                if isConfirmed{
                    self?.showHud()
                    ApiServices.shared.DeactivateCard(cardId: self?.selectedCard?.cardId ?? 0) { (statusCode) in
                        self?.hideHUD()
                        if statusCode == 200 {
                            Shared.logOut()
                            Shared.showToast(text: "Card Deactivated Successfully")
                        }
                    }
                }
            }
        }
        //
        
        initializeView()
    }
    
    private func setupFreezeButton(isFreezed: Bool){
        freezeLabel.text = "\(isFreezed ? "Unfreeze" : "Freeze") Card"
        freezeImageView.image = UIImage(named: isFreezed ? "unfreeze" : "freezeCard")
        freezeView.addTapGesture { [weak self] in
            AlertConfirmPresenter(description: "Are you sure you want to \((self?.selectedCard?.isFreezed ?? false) ? "Unfreeze" : "Freeze") your card ?", confirmTitle: "Yes", rejectTitle: "Cancel") { (isConfirmed) in
                if isConfirmed{
                    self?.showHud()
                    ApiServices.shared.ChangeFreezeState(cardId: self?.selectedCard?.cardId ?? 0, freeze: !(self?.selectedCard?.isFreezed ?? false)) { (statusCode) in
                        self?.hideHUD()
                        if statusCode == 200 {
                            self?.setUpPage(showLoader: false)
                        }
                    }
                }
            }
        }
    }
    
    private func initializeView(){
        currentTabSelected = CardsPageTabs.CardInfo.rawValue
    }
    private func changeSelectedTab(selectedTab: Int){
        let isInfo = selectedTab == CardsPageTabs.CardInfo.rawValue
        let isPayments = selectedTab == CardsPageTabs.Payments.rawValue
        let isTrasfers = selectedTab == CardsPageTabs.Trasnfers.rawValue
        //change selected button
        changeTabButtonDisplay(button: cardTabButtton, isSelected: isInfo)
        changeTabButtonDisplay(button: paymentsTabButton, isSelected: isPayments)
        changeTabButtonDisplay(button: transfersTabButton, isSelected: isTrasfers)
        //change displayed views
        cardInforHolderView.isHidden = !isInfo
        paymentsHolderView.isHidden = !isPayments
        trasnfersHolderView.isHidden = !isTrasfers
    }
    private func changeTabButtonDisplay(button: GradientButton, isSelected: Bool){
        button.startColor = isSelected ? UIColor.gradientSecondColor : UIColor.fadedButtonColor
        button.endColor = isSelected ? UIColor.gradientFirstColor : UIColor.fadedButtonColor
        button.setTitleColor(isSelected ? UIColor.white: UIColor.textFadedColor, for: .normal)
        button.titleLabel?.font = UIFont(name: isSelected ? "FiraSans-Medium" : "FiraSans-Light", size: 16)
    }
    private func changePageDisplay(HaveCards: Bool){
        noDataView.isHidden = HaveCards
        noCardsImageView.isHidden = HaveCards
        cardsCollectionView.isHidden = !HaveCards
        for label in infoLabels ?? [] {
            label.textColor = HaveCards ? .black : .fadedButtonColor
        }
        
        for image in infoImages ?? [] {
            image.tintColor = HaveCards ? .kehle : .fadedButtonColor
        }
        for view in infoTabViews ?? []{
            view.isUserInteractionEnabled = HaveCards
        }
    }
    @IBAction func tabButtonClicked(_ sender: UIButton) {
        currentTabSelected = sender.tag
    }
}

extension MainViewController: CardsCollectionDelegate {
    func didSelectItem(item: CardModel) {
        LocalAuthenticationHelper() { (isSuccess) in
            if isSuccess{
                let vc = ScreenLoader().popUpHalfView(mainView: self, popUpVc: CardInfo_VC, height: screenHeight - screenHeight/5) as! CardInfoViewController
                vc.cardID = item.cardId ?? 0
            }
        }
    }
    
    func currentPageChanged(index: Int) {
        currentPage = index
        selectedCard = cards[index]
    }
}
extension MainViewController: ClaimCardDelegate{
    func cardClaimed() {
        setUpPage()
    }
}
