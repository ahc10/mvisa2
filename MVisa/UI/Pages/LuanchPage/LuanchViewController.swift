//
//  LuanchViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 6/29/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class LuanchViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        perform(#selector(navigate), with: nil, afterDelay: 2)
        
    }
    
    
    @objc func navigate() {
        AppDelegate.shared.startApp()
    }
    
}
