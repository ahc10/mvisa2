//
//  CardInfoViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/7/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class CardInfoViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var holderNameLabel: UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var cardTitleLabel: UILabel!
    @IBOutlet weak var cvvTrailingSpaceContrainet: NSLayoutConstraint!
    var cardID: Int = 0{
        didSet{
            setUpPage()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        startSkeletonAnimation(view: self.view)
   
        self.view.roundCorners(corners: [.topLeft, .topRight], radius: 20.0)
        switch deviceSize {
        case .small:
            cvvTrailingSpaceContrainet.constant = -(screenWidth - 80)/5
        case .Medium:
            cvvTrailingSpaceContrainet.constant = -(screenWidth - 80)/5
        default:
            cvvTrailingSpaceContrainet.constant = -(screenWidth - 80)/6
        }
        titleLabel.text = "View Card Information"
        cancelButton.setTitle("Cancel", for: .normal)
        cardTitleLabel.text = "Virtual Credit Card"
    }
    override func setUpPage(showLoader: Bool = true) {
        super.setUpPage(showLoader: showLoader)
        ApiServices.shared.getCardInfo(cardId: cardID) { (result, statusCode) in
               self.hideSkeletonAnimation(view: self.view)
               if statusCode == 200 {
                   let card = result.card
                   self.cardNumberLabel.text = card?.card_number
                   self.holderNameLabel.text = card?.card_holder_name
                   self.expiryLabel.text = card?.expiry_date
                   self.cvvLabel.text = card?.cvv
               }
           }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismissSemiModalView()
    }
    

}
