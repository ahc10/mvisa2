//
//  MyProfileViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var firstNameTitleLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTitleLabel: UILabel!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var mobileNumberTitleLabel: UILabel!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var passwordtextField: UITextField!
    @IBOutlet weak var passwordTitleLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "My Profile"
        descLabel.text = "you can only view your profile & change your password"
        backButton.setTitle("Back", for: .normal)
        changePasswordButton.setTitle("Change Password", for: .normal)
        
        firstNameTitleLabel.text = "First Name"
        lastNameTitleLabel.text = "Last Name"
        mobileNumberTitleLabel.text = "Mobile Number"
        passwordTitleLabel.text = "Password"
        
        firstNameTextField.text = UserInfoDefualts.firstName
        lastNameTextField.text = UserInfoDefualts.lastName
        mobileNumberTextField.text = UserInfoDefualts.phoneNumber
        passwordtextField.text = "********"
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
         self.closeView.addGestureRecognizer(gesture)
    }
    
    @objc private func closePage(){
        self.dismiss(animated: true) {
        }
    }
    @IBAction func backClicked(_ sender: Any) {
        closePage()
    }
    @IBAction func changePasswordClicked(_ sender: Any) {
        ScreenLoader().popUpView(mainView: self, popUpVc: ChangePassword_VC)
        
    }
}
