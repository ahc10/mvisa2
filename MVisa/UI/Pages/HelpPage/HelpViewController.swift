//
//  HelpViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class HelpViewController: UIViewController {
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var requestTitleLabel: UILabel!
    @IBOutlet weak var titletextField: UITextField!
    @IBOutlet weak var messageTextView: KMPlaceholderTextView!
    @IBOutlet weak var submitButton: GradientButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Help"
        descLabel.text = "To submit your question please fill all required fields"
        requestTitleLabel.text = "Request Title"
        titletextField.placeholder = "Enter Title"
        messageTextView.placeholder = "Write your message"
        submitButton.setTitle("Submit", for: .normal)
        submitButton.changePrimaryButtonState(isEnabled: false)
        cancelButton.setTitle("Cancel", for: .normal)
        
        titletextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        messageTextView.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        updateButtonState()
        
    }
    private func updateButtonState(){
        let title = titletextField.text ?? ""
        let message = messageTextView.text ?? ""
        
        if title == "" || message == "" {
            submitButton.changePrimaryButtonState(isEnabled: false)
        }else{
            submitButton.changePrimaryButtonState(isEnabled: true)
        }
    }
    
    @objc private func closePage(){
        self.dismiss(animated: true) {}
    }
    @IBAction func backCLicked(_ sender: Any) {
        closePage()
    }
    @IBAction func sumbitClicked(_ sender: Any) {
        let title = titletextField.text ?? ""
        let message = messageTextView.text ?? ""
        self.showHud()
        ApiServices.shared.contactUs(title: title, message: message) { (statusCode) in
            self.hideHUD()
            if statusCode == 200 {
                self.dismiss(animated: true) {
                    Shared.showToast(text: "Message Submitted Successfully")
                }
            }
        }
    }
}
extension HelpViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        updateButtonState()
    }
}
