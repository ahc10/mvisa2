//
//  PayViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/15/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

enum PayPageType: Int {
    case button = 0
    case touchID = 1
}
class PayViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var payButton: GradientButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var touchidLabel: UILabel!
    @IBOutlet weak var payButtonView: UIView!
    @IBOutlet weak var payTouchView: UIView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    var paymentData: QRData?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        payButton.setTitle("Pay Now", for: .normal)
        cancelButton.setTitle("Cancel", for: .normal)
        touchidLabel.text = "Pay with touch ID"
        totalLabel.text = "Total Amount"
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
        
    }
    
    public func setupPage(title: String, paymentData: QRData, type: PayPageType){
        self.paymentData = paymentData
        let amount = "\(paymentData.transferAmount ?? 0) \(paymentData.currency ?? "")"
        if type == .button {
            payButtonView.isHidden = false
            payTouchView.isHidden = true
        }else{
            payButtonView.isHidden = true
            payTouchView.isHidden = false
        }
        titleLabel.text = title
        amountLabel.text = amount
        
        let string = NSMutableAttributedString(string: "\(amount) will be deducted from your balance are you sure you want to proceed  ?")
        string.setFontForText(String(amount), with: UIFont(name: "FiraSans-Bold", size: 19.0)!)
        descLabel.attributedText = string
    }
    @objc private func closePage(){
        self.dismiss(animated: true) {}
    }
    @IBAction func cancelClicked(_ sender: Any) {
        closePage()
    }
    
    @IBAction func payClicked(_ sender: Any) {
        LocalAuthenticationHelper() { (isSuccess) in
                if isSuccess{
                  self.showHud()
                    ApiServices.shared.payAmount(data: self.paymentData ?? QRData()) { (statusCode) in
                       self.hideHUD()
                       if statusCode == 200 {
                        NotificationCenter.default.post(name: .updateMainPageData, object: nil)
                           self.dismiss(animated: true) {
                               let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: TopUpSuccess_VC) as! TopUpSuccessViewController
                               vc.setText(title: "Payment", description: "Your payment \(self.paymentData?.transferAmount ?? 0) \(self.paymentData?.currency ?? "") has been successfully made")
                           }
                       }
                   }
                }
            }
    }
}
