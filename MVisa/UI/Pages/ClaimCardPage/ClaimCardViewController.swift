//
//  ClaimCardViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/7/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit
protocol ClaimCardDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func cardClaimed()
}
class ClaimCardViewController: BaseViewController {
    public weak var delegate: ClaimCardDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var claimButton: GradientButton!
    @IBOutlet weak var cardImageView: UIImageView!
    
    var canClaim: Bool = true{
        didSet{
            if (viewIfLoaded != nil){
                setUpPage()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPage()
        
    }
    
    public func setUpPage(){
        cardImageView.loadGif(name: canClaim ? "scanCard" : "putCard")
        titleLabel.text = canClaim ? "Thank you !" : "We're sorry !"
        
        let boldText = "Virtual Credit Card"
        let string = NSMutableAttributedString(string: canClaim ? "Your Virtual Credit Card is ready would \n you like to claim it?" : "You will not be able to receive your \n Virtual Credit Card at this stage")
        string.setFontForText(String(boldText), with: UIFont(name: "FiraSans-Bold", size: 19.0)!)
        descLabel.attributedText = string
        
        claimButton.setTitle(canClaim ? "Claim Card" : "I understand", for: .normal)
    }
    
    
    @IBAction func claimClicked(_ sender: Any) {
        if canClaim{
            self.showHud()
            ApiServices.shared.claimCard { (result, statusCode) in
                self.hideHUD()
                
                if statusCode == 200 {
                    self.dismiss(animated: true) {
                        self.delegate?.cardClaimed()
                    }
                }
            }
        }else{
            self.dismiss(animated: true) {

            }
        }
        
    }
}
