//
//  PayOptionsViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

enum PayOptions: Int {
    case NFC = 0
    case QR = 1
    case None = 2
}
class PayOptionsViewController: BaseViewController {
    
    @IBOutlet weak var nfcButton: GradientButton!
    @IBOutlet weak var qrButton: GradientButton!
    @IBOutlet weak var nextButton: GradientButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var closeView: UIView!
    
    var selectedOption: Int = PayOptions.None.rawValue {
        didSet{
            if selectedOption == TopUpOptions.None.rawValue{
                nextButton.changePrimaryButtonState(isEnabled: false)
                changeButtonOption(button: nfcButton, isSelected: false)
                changeButtonOption(button: qrButton, isSelected: false)
                
            }else if selectedOption == PayOptions.NFC.rawValue {
                nextButton.changePrimaryButtonState(isEnabled: true)
                changeButtonOption(button: qrButton, isSelected: false)
                changeButtonOption(button: nfcButton, isSelected: true)
                
            }else if selectedOption == PayOptions.QR.rawValue{
                nextButton.changePrimaryButtonState(isEnabled: true)
                changeButtonOption(button: qrButton, isSelected: true)
                changeButtonOption(button: nfcButton, isSelected: false)
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "Pay"
        descLabel.text = "Choose one of the following methods "
        cancelButton.setTitle("Cancel", for: .normal)
        nextButton.setTitle("Next", for: .normal)
        
        nfcButton.setTintedImage(named: "nfc", state: .normal)
        nfcButton.setTitle("Tap to Pay", for: .normal)
        nfcButton.alignImageAndTitleVertically()
        
        qrButton.setTintedImage(named: "qr", state: .normal)
        qrButton.setTitle("Scan QR code", for: .normal)
        qrButton.alignImageAndTitleVertically()
        
        selectedOption = TopUpOptions.None.rawValue
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
        
    }
    
    private func changeButtonOption(button: GradientButton, isSelected: Bool){
        button.tintColor = isSelected ? UIColor.white : UIColor.kehle
        button.setTitleColor(isSelected ? UIColor.white : UIColor.kehle, for: .normal)
        if isSelected{
            button.startColor = .kehle
            button.endColor = .kehleLight
            
        }else{
            button.startColor = .white
            button.endColor = .white
        }
    }
    private func changeNextButtonState(isEnabled: Bool){
        nextButton.isEnabled = isEnabled
        nextButton.setTitleColor(isEnabled ? UIColor.white : UIColor.black, for: .normal)
        if isEnabled {
            nextButton.startColor = UIColor.gradientSecondColor
            nextButton.endColor = UIColor.gradientFirstColor
        }else{
            nextButton.startColor = UIColor.fadedButtonColor
            nextButton.endColor = UIColor.fadedButtonColor
        }
    }
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func optionClicked(_ sender: UIButton) {
        selectedOption = sender.tag
    }
    @IBAction func closeClicked(_ sender: Any) {
        closePage()
    }
    @IBAction func nextClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            
            if self.selectedOption == PayOptions.QR.rawValue{
                ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc:QRScan_VC)
                
            }else if self.selectedOption == PayOptions.NFC.rawValue{
                let data = QRData()
                data.currency = "USD"
                data.toAccountId = 346
                data.toClientId = 733
                data.transferAmount = 10
                let vc = ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: Pay_VC) as! PayViewController
                vc.setupPage(title: "Payment Information", paymentData: data, type: .button)
                
            }
        }
        
    }
}

