//
//  TopUpOptionsViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/8/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

enum TopUpOptions: Int {
    case Scratch = 0
    case Transfer = 1
    case None = 2
}
class TopUpOptionsViewController: BaseViewController {
    
    @IBOutlet weak var scratchButton: GradientButton!
    @IBOutlet weak var transferBUtton: GradientButton!
    @IBOutlet weak var nextButton: GradientButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var closeView: UIView!
    
    var selectedOption: Int = TopUpOptions.None.rawValue {
        didSet{
            if selectedOption == TopUpOptions.None.rawValue{
                nextButton.changePrimaryButtonState(isEnabled: false)
                changeButtonOption(button: transferBUtton, isSelected: false)
                changeButtonOption(button: scratchButton, isSelected: false)
                
            }else if selectedOption == TopUpOptions.Scratch.rawValue {
                nextButton.changePrimaryButtonState(isEnabled: true)
                changeButtonOption(button: transferBUtton, isSelected: false)
                changeButtonOption(button: scratchButton, isSelected: true)
                
            }else if selectedOption == TopUpOptions.Transfer.rawValue{
                nextButton.changePrimaryButtonState(isEnabled: true)
                changeButtonOption(button: transferBUtton, isSelected: true)
                changeButtonOption(button: scratchButton, isSelected: false)
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "Top Up"
        descLabel.text = "Choose one of the following methods "
        cancelButton.setTitle("Cancel", for: .normal)
        nextButton.setTitle("Next", for: .normal)
        scratchButton.setTintedImage(named: "scratchTopUp", state: .normal)
        scratchButton.setTitle("Scratch Card", for: .normal)
        scratchButton.alignImageAndTitleVertically()
        
        transferBUtton.setTintedImage(named: "transferTopUp", state: .normal)
        transferBUtton.setTitle("Transfer", for: .normal)
        transferBUtton.alignImageAndTitleVertically()
        
        selectedOption = TopUpOptions.None.rawValue
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
        
    }
    
    private func changeButtonOption(button: GradientButton, isSelected: Bool){
        button.tintColor = isSelected ? UIColor.white : UIColor.kehle
        button.setTitleColor(isSelected ? UIColor.white : UIColor.kehle, for: .normal)
        if isSelected{
            button.startColor = .kehle
            button.endColor = .kehleLight
            
        }else{
            button.startColor = .white
            button.endColor = .white
        }
    }
    private func changeNextButtonState(isEnabled: Bool){
        nextButton.isEnabled = isEnabled
        nextButton.setTitleColor(isEnabled ? UIColor.white : UIColor.black, for: .normal)
        if isEnabled {
            nextButton.startColor = UIColor.gradientSecondColor
            nextButton.endColor = UIColor.gradientFirstColor
        }else{
            nextButton.startColor = UIColor.fadedButtonColor
            nextButton.endColor = UIColor.fadedButtonColor
        }
    }
    @objc private func closePage(){
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func optionClicked(_ sender: UIButton) {
        selectedOption = sender.tag
    }
    @IBAction func closeClicked(_ sender: Any) {
        closePage()
    }
    @IBAction func nextClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            
            if self.selectedOption == TopUpOptions.Scratch.rawValue{
                ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: ScratcCard_VC)
                
            }else{
                ScreenLoader().popUpView(mainView: UIApplication.getTopViewController()!, popUpVc: ConfirmAirtimeTransfer_VC)
            }
        }
        
    }
}
