//
//  TermsViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 6/4/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class TermsViewController: BaseViewController {
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var htmlTextView: UITextView!
    @IBOutlet weak var topView: GradientView!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
        if deviceSize == DeviceSize.small{
               topViewHeight.constant = 130
           }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        isLightStatus = true
        titleLabel.text = "Terms & Conditions"
        ApiServices.shared.getTerms { (result, statusCode) in
            if statusCode == 200 {
                let terms = result.text ?? ""
                self.htmlTextView.attributedText = (terms).htmlToAttributedString
            }
        }
    }    
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
