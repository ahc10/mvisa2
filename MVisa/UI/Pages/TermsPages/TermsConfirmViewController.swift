//
//  TermsConfirmViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 7/10/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class TermsConfirmViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var htmlTextView: UITextView!
    @IBOutlet weak var acceptButton: GradientButton!
    @IBOutlet weak var holderView: UIView!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptButton.setTitle("I Accept The Terms & Conditions", for: .normal)
        acceptButton.isEnabled = false
        titleLabel.text = "Terms & Conditions"
        setUpPage()
    }
    
    override func setUpPage(showLoader: Bool = true) {
        self.showHud()
        ApiServices.shared.getTerms { (result, statusCode) in
            self.hideHUD()
            if statusCode == 200 {
                self.acceptButton.isEnabled = true
                let terms = result.text ?? ""
                self.htmlTextView.attributedText = (terms).htmlToAttributedString
            }
        }
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        isTermsAcceptedDefualts = true
        self.dismiss(animated: true) {
        }
    }
}
