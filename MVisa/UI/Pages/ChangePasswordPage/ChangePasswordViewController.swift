//
//  ChangePasswordViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/11/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var currentTitle: UILabel!
    @IBOutlet weak var currentTextView: PasswordTextView!
    @IBOutlet weak var newTitleLabel: UILabel!
    @IBOutlet weak var newTextView: PasswordTextView!
    @IBOutlet weak var repeatTitleLabel: UILabel!
    @IBOutlet weak var repeatTextView: PasswordTextView!
    @IBOutlet weak var confirmButton: GradientButton!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        holderView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.setTitle("Cancel", for: .normal)
        titleLabel.text = "Change Password"
        descLabel.text = "To change password please fill all required fields"
        currentTitle.text = "Current Password"
        currentTextView.textField.placeholder = "Enter Current Password"
        newTitleLabel.text = "New Password"
        newTextView.textField.placeholder = "Enter New Password"
        repeatTitleLabel.text = "Repeat New Password"
        repeatTextView.textField.placeholder = "Reapeat Password"
        confirmButton.setTitle("Confirm", for: .normal)
        confirmButton.changePrimaryButtonState(isEnabled: false)
        
        currentTextView.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        newTextView.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        repeatTextView.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.closePage))
        self.closeView.addGestureRecognizer(gesture)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let old = currentTextView.textField.text ?? ""
        let new = newTextView.textField.text ?? ""
        let confirm = repeatTextView.textField.text ?? ""
        if old == "" || new == "" || confirm == ""{
            confirmButton.changePrimaryButtonState(isEnabled: false)
        }else{
            confirmButton.changePrimaryButtonState(isEnabled: true)
        }

    }
    @objc private func closePage(){
        self.dismiss(animated: true) {}
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        closePage()
    }
    @IBAction func confirmClicked(_ sender: Any) {
        let old = currentTextView.textField.text ?? ""
          let new = newTextView.textField.text ?? ""
          let confirm = repeatTextView.textField.text ?? ""
        if new != confirm {
            Shared.displayAlert(title: "Password Match!", message: "new password and confirm password doesn't match!", cancelText: "Done")
        }else{
            self.showHud()
            ApiServices.shared.changePassword(oldPass: old, newPass: new) { (statusode) in
                self.hideHUD()
                if statusode == 200 {
                    self.dismiss(animated: true) {
                        Shared.showToast(text: "Password Changed Successfully")
                    }

                }
            }
        }
    }
}
