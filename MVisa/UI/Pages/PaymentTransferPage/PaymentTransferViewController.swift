//
//  PaymentTransferViewController.swift
//  MVisa
//
//  Created by Ali Merhie on 5/7/20.
//  Copyright © 2020 Monty Mobile. All rights reserved.
//

import UIKit

class PaymentTransferViewController: BaseViewController {
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topView: GradientView!
    @IBOutlet weak var dateFilterView: DateFilterDisplayView!
    @IBOutlet weak var chooseDateButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    private var paymentsTablePresenter: PaymentsTablePresenter?
    private var transferTablePresenter: TransferTablePresenter?
    var fromDate: String?
    var toDate: String?

    var isPaymentsDisplay = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
        if deviceSize == DeviceSize.small{
            topViewHeight.constant = 130
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        isLightStatus = true
        dateFilterView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(updateDataOnPaymentLive), name: .updateMainPageData, object: nil)

        titleLabel.text = isPaymentsDisplay ? "Payments" : "Top Up"
        chooseDateButton.setTitle("Choose a date", for: .normal)
        if isPaymentsDisplay{
            paymentsTablePresenter = PaymentsTablePresenter(tableView: tableView, haveMoreButton: false)
            paymentsTablePresenter?.items = []
        }else{
            
            transferTablePresenter = TransferTablePresenter(tableView: tableView, haveMoreButton: false)
            transferTablePresenter?.items = []
        }
        dateFilterView.setSingleDate(date: Date())

       // Shared.startSkeletonAnimation(view: self.tableView)

    }
    @objc private func updateDataOnPaymentLive(){
        updatePageInfo(fromDate: self.fromDate ?? "", toDate: self.toDate ?? "", showLoader: false)
    }
     private func updatePageInfo(fromDate: String, toDate: String, showLoader: Bool = true){
        if isPaymentsDisplay{
            if showLoader{
            startSkeletonAnimation(view: self.view)
            }
            ApiServices.shared.getPaymentts(from: fromDate, to: toDate) { (result, statusCode) in
                self.hideSkeletonAnimation(view: self.view)
                if statusCode == 200 {
                    self.paymentsTablePresenter?.items = result.payments
                }
            }
        }else{
            if showLoader{
            startSkeletonAnimation(view: self.view)
            }
            ApiServices.shared.getTrasnfers(from: fromDate, to: toDate) { (result, statusCode) in
                self.hideSkeletonAnimation(view: self.view)
                if statusCode == 200 {
                    self.transferTablePresenter?.items = result.trasnfers
                }
            }
        }
    }
    @IBAction func chooseDateClicked(_ sender: Any) {
        dateFilterView.displayDateSelectionPage()
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension PaymentTransferViewController: DateFilterDelegate {
    func dateSelected(fromDate: String, toDate: String) {
        self.fromDate = fromDate
        self.toDate = toDate
        updatePageInfo(fromDate: fromDate, toDate: toDate)
    }
}
